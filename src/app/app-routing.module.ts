// --- Application imports
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './shared/components/login/login.component';
import {SingupComponent} from './shared/components/singup/singup.component';
import {PassresetComponent} from './shared/components/login/passreset/passreset.component';
import { CmsModule } from './cms/cms.module';


const routes: Routes = [
{
    path: '',
    component: LoginComponent
},
{
    path: 'registracija',
    component: SingupComponent
},
{
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
},
{
    path: 'user',
    loadChildren: './user/user.module#UserModule'
},
{
    path: 'cms',
    loadChildren: './cms/cms.module#CmsModule'
},
{
    path: 'cms',
    loadChildren: () => import ('./cms/cms.module').then(m=> m.CmsModule)
},
{
    path: 'respass',
    component: PassresetComponent
}

// { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})

export class AppRoutingModule {}

