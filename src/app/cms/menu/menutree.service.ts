import { Injectable, ɵNG_INJ_DEF } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { FlatTreeControl } from '@angular/cdk/tree';
import { ILink, IFlatLink } from '../shared/models/links/links';
import { CdkDragDrop } from '@angular/cdk/drag-drop';


@Injectable({
  providedIn: 'root'
})
export class MenutreeService {

  constructor() { }

  private subTeeData$ = new BehaviorSubject<ILink[]>([]);
  treeData$ = this.subTeeData$.asObservable();

  setTreeData(data) { this.subTeeData$.next(data); }

  transformer = (node: ILink, level: number): IFlatLink => {
    if (node.inmenu) {
      return <IFlatLink>{
        id_link: node.id_link,
        link_name: node.link_name,
        id_parent_link: node.inmenu.id_parent_link,
        level: node.inmenu.row,
        position: node.inmenu.position,
        expandable: node.inmenu.expandable,
      }
    } else { console.log('Ne pravi'); }
  }

  getLevel = (node: IFlatLink) => node.level;
  isExpandable = (node: IFlatLink) => node.expandable;
  getChildren = (node: ILink): ILink[] => node.children;

  buildFileTree(obj: { [key: string]: any }, row: number, position: number = 0, parentId?: number): ILink[] {
    return null;
    // return Object.keys(obj).reduce<ILink[]>((accumulator, key, idx) => {
    // const value = obj[key];
    // const node = new FileNode();
    // node.filename = key;
    /**
     * Make sure your node has an id so we can properly rearrange the tree during drag'n'drop.
     * By passing parentId to buildFileTree, it constructs a path of indexes which make
     * it possible find the exact sub-array that the node was grabbed from when dropped.
     */
    // node.id = `${parentId}/${idx}`;

    // if (value != null) {
    //   if (typeof value === 'object') {
    //     node.children = this.buildFileTree(value, level + 1, node.id);
    //   } else {
    //     node.type = value;
    //   }
    // }

    // return accumulator.concat(node);
    // }, []);
  }

  private getParentNode(treeControl: FlatTreeControl<IFlatLink>, node: ILink): IFlatLink | null {
    const currentLevel = node.inmenu.row;
    if (currentLevel < 1) {
      return null;
    }
    const startIndex = treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = treeControl.dataNodes[i];
      if (currentNode.level < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  rememberExpandedTreeNodes(treeControl: FlatTreeControl<IFlatLink>, expandedNodeSet: Set<number | string>) {
    if (treeControl.dataNodes) {
      treeControl.dataNodes.forEach((node) => {
        if (treeControl.isExpandable(node) && treeControl.isExpanded(node)) {
          // capture latest expanded state
          expandedNodeSet.add(node.id_link);
        }
      });
    }
  }


  private forgetMissingExpandedNodes(
    treeControl: FlatTreeControl<IFlatLink>,
    expandedNodeSet: Set<string>
  ) {
    if (treeControl.dataNodes) {
      expandedNodeSet.forEach((nodeId) => {
        // maintain expanded node state
        if (!treeControl.dataNodes.find((n) => n.domId === nodeId)) {
          // if the tree doesn't have the previous node, remove it from the expanded list
          expandedNodeSet.delete(nodeId);
        }
      });
    }
  }

  moveListMenu(event: CdkDragDrop<string[]>, arryList: ILink[], arryMenu: ILink[]) {
    console.log('Tu sam');
    const previousContainer = event.previousContainer.id === 'linkList' ? arryList : arryMenu;
    const newContainer = event.container.id === 'linkList' ? arryList : arryMenu;
    this.initLinkInMenu(previousContainer[event.previousIndex], true, 0, newContainer.length);
    newContainer.push(previousContainer.splice(event.previousIndex, 1)[0]);
  }

  initLinkInMenu(link: ILink, exp = true, level = 0, position = 0, parentId: number = 0) {
    if (!link.inmenu) {
      link.inmenu = {
        id_link: link.id_link,
        id_parent_link: parentId,
        row: level, position: position,
        domId: `${parentId}/${position}`,
        expandable: exp,
        isParent: true
      };
    }
    else {
      link.inmenu.id_parent_link = parentId;
      link.inmenu.expandable = exp;
      link.inmenu.row = level;
      link.inmenu.position = position;
      link.inmenu.domId = `${parentId}/${position}`;
      link.inmenu.isParent = true;
    }
    this.initChildrenInLink(link);
  }

  initChildrenInLink(link: ILink) {
    if (link && !link.children) {
      link.children = [{
        id_link: -(link.id_link),
        link_name: 'DODAJ',
        parent: 'link',
        inmenu: { id_parent_link: link.id_link, row: link.inmenu.row + 1, position: 0, domId: `${link.id_link}/0`, expandable: false, isParent: false }
      }];
    } else {
      link.children.push({
        id_link: -(link.id_link),
        link_name: 'DODAJ',
        parent: 'link',
        inmenu: { id_parent_link: link.id_link, row: link.inmenu.row + 1, position: link.children.length, domId: `${link.id_link}/0`, expandable: false, isParent: false }
      });
    }
  }



}
