import { Injectable } from '@angular/core';
import { ICrudService } from '../../shared/models/interfaces/common';
import { Observable } from 'rxjs';
import { RequestService } from 'src/app/shared/services/request.service';
import { APPURL } from 'src/app/shared/globals';
import { map } from 'rxjs/operators';
import { ILink } from '../../shared/models/links/links';
import { MatPaginator } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class LinkService implements ICrudService {

  constructor(private rs: RequestService) { }
  newData(dataObject: any): Observable<any> {
    return this.rs.post(APPURL + '/link', dataObject).pipe(
      map(data => <ILink>data)
    );
  }
  updateData(dataObject: any, id: number): Observable<any> {
    return this.rs.put(APPURL + '/link/' + id, dataObject).pipe(
      map((link: ILink) => link)
    );
  }
  getData(pagination: boolean, filter?: any): Observable<any> {
    return this.rs.get(APPURL + '/link').pipe(
      map(data => <ILink[]>data['data']));
  }

  removeLinkTekst(idLink, idText): Observable<any> {
    return this.rs.put(APPURL + '/linkremovetext/' + idLink, idText).pipe(
      map((links:ILink)=>links)
    )
  }

  paginatorUpdate(data: any) {
    this.rs.setPaginatorData(data);
  }
  initPaginator(paginator: MatPaginator, data: any) {
    this.rs.setPaginator(paginator, data);
  }
}
