import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { IBaseFunctionality, IBasePagination, IBaseTab } from '../../shared/models/interfaces/common';
import { FormBuilder, Validators, FormGroupDirective, FormControl, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { MatSnackBar, MatTabGroup, MatSelect, MatSelectionList, MatPaginator, MatChip } from '@angular/material';
import { ObjLineText, TEXTTYPE } from '../../shared/models/texts/text';
import { map } from 'rxjs/operators';
import { LangService } from '../../settings/languages/lang.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { ILanguage } from '../../shared/models/langauge/langauge';
import { USERROLES, USERROLE } from 'src/app/shared/models/user';
import { LinkService } from './link.service';
import { DataSource } from '@angular/cdk/table';
import { ILink, LINK_PARENT } from '../../shared/models/links/links';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent extends BasecomponentComponent
  implements OnInit, AfterViewInit, IBaseFunctionality, IBasePagination, IBaseTab {


  //Observable Section
  languages$: Observable<ILanguage[]>;

  // Property Section
  @ViewChild('langSelect', { static: true }) langSelect: MatSelect;
  @ViewChild('linkTab', { static: true }) tabGroup: MatTabGroup;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  updateModel: ILink = null;
  texttype = TEXTTYPE;
  userroles = USERROLE;
  PARENT = LINK_PARENT;
  selectedTab = 1;
  option: string;
  isLoading = false;


  langSelection = [];

  newLink = this.fb.group({
    link_name: ['', [Validators.required]],
    link_auth: [''],
    text_type: ['line', [Validators.required]],
    paragraphs: this.fb.array([]),
    text_paragraphs: ['', Validators.required],
    languages: ['', Validators.required],
    parent: [this.PARENT, [Validators.required]]
  });
  
  //  Data Table
  subTableData$ = new BehaviorSubject([]);
  tableData$ = new ContentDataSource(this.subTableData$.asObservable());
  headerColumns = ['position', 'link_name', 'link_auth', 'text', 'edit'];
  //

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    protected fus: FileuploadService,
    protected snackBar: MatSnackBar,
    private lgs: LangService,
    private lns: LinkService
  ) { super(fus, snackBar); }

  ngOnInit(): void {
    this.selSelectedTab();
    this.languages$ = this.lgs.getData(false).pipe(
      map(data => data['data'])
    );
  }

  ngAfterViewInit(): void {
    this.onTabSelect();

    this.langSelect.optionSelectionChanges.subscribe(
      language => {
        if (language.isUserInput) {
          if (language.source.selected === true) {
            const newLineText = {
              id_language: { value: language.source.value.id_language, disabled: false },
              text_type: { value: 'line', disabled: false },
              title_par: { value: '', disabled: false },
              text: { value: '', disabled: false }
            };
            super.addToFormArray(newLineText, this.newLink.get('paragraphs'));
          }
          else {
            this.newLink.get('paragraphs').value.map((control, i) => {
              if (language.source.value.id_language === control.id_language) {
                const textId = (<FormArray>this.newLink.get('paragraphs')).controls[i].get('id_text');
                if (textId) {
                  confirm('Da li ste sigurni da želite da uklonite tekst za jezik?');
                  this.lns.removeLinkTekst(this.updateModel.id_link, { id_text: textId.value }).subscribe(result =>
                    super.resolveResult(result, 'Sadržaj je obrisan.')
                  );
                }
                super.removeFromArr(<FormArray>this.newLink.get('paragraphs'), i);
                console.log(i);
              }
            });
            // this.newLink.get('paragraphs')[0].controls.map(dara => console.log(dara))
          }
        }
      });
  }

  selectTab(tab: string): void {
    if (tab === 'novi') {
      this.tabGroup.selectedIndex = 0;
    }
    // tslint:disable-next-line:one-line
    else if (tab === 'list') {
      if (this.newLink.dirty) {
        this.onResetFrom();
      }
      this.tabGroup.selectedIndex = 1;
    }
    // tslint:disable-next-line:one-line
    else { this.selectedTab = 0; }
  }

  selSelectedTab(): void {
    this.route.params.subscribe(
      data => {
        if (data['option']) {
          this.option = data['option'];
          this.selectTab(this.option);
        }
      });
  }

  onTabSelect(): void {
    this.tabGroup.selectedIndexChange.subscribe(data => {
      if (data === 1) {
        this.option = 'list'; this.selectTab(this.option);
        this.onResetDataTable();
      }
      if (data === 0) { this.option = 'novi'; this.selectTab(this.option); }
    });
  }

  onFormSubmit(formGroup?: FormGroupDirective): void {
    this.newLink.get('text_paragraphs').setValue(JSON.stringify(this.newLink.get('paragraphs').value));
    if (this.newLink.valid && this.newLink.dirty) {
      this.isLoading = true;
      console.log(this.newLink.value);
      this.lns.newData(this.newLink.value).subscribe(result => {
        super.resolveResult(result, 'Uspešno ste dodali novi link.');
      });
    }
  }

  onFormUpdate(formGroup?: FormGroupDirective): void {
    this.newLink.get('text_paragraphs').setValue(JSON.stringify(this.newLink.get('paragraphs').value));
    if (this.newLink.valid && this.newLink.dirty) {
      this.lns.updateData(this.newLink.value, this.updateModel.id_link).subscribe(result =>
        super.resolveResult(result, 'Uspešno ste izmenili link')
      )
    }
  }

  onResetFrom(formGroup?: FormGroupDirective): void {
    this.updateModel ? this.updateModel = null : false;
    formGroup ? formGroup.reset() : false;
    this.newLink.reset();
    this.newLink.get('text_type').setValue('line');
    this.langSelect.value = '';
    super.removeFromArrayLimit(<FormArray>this.newLink.get('paragraphs'), 0)

    this.isLoading === true ? this.isLoading = false : false;
  }

  private populateLinkForm(model: ILink) {
    console.log(model);
    this.newLink.get('link_name').setValue('Milos');
    this.newLink.get('link_auth').setValue(USERROLES['2']);
    this.langSelect.options.map(data => {
      if (data && data.value.id_language) {
        model.texts.map(text => {
          if (data.value.id_language === text.id_language) {
            data.select();
            const newLineText = {
              id_language: { value: text.id_language, disabled: false },
              text_type: { value: 'line', disabled: false },
              title_par: { value: text.text_paragraphs['tilte_par'], disabled: false },
              text: { value: text.text_paragraphs['text'], disabled: false },
              id_text: { value: text.id_text, disabled: false }
            };
            console.log(newLineText);
            super.addToFormArray(newLineText, this.newLink.get('paragraphs'));
          }
        });
      }
    });
    this.newLink.get('parent').setValue(this.PARENT);
  }


  modelForUpdate(link: ILink) {
    if (link) {
      this.updateModel = link;
      this.updateModel.parent = LINK_PARENT;
      console.log(this.updateModel);
      this.newLink.markAsDirty();
      this.populateLinkForm(this.updateModel);
      this.option = 'novi';
      this.selectTab(this.option);
    }
  }

  onPaginationChange(event: import("@angular/material").PageEvent): void {
    throw new Error("Method not implemented.");
  }

  onResetDataTable(first = true) {
    this.isLoading = true; // Progress bar trigger
    if (!super.firstPagePagination(this.paginator, first)) { // List reload on selected page
      this.lns.getData(true).pipe(
        map(data => data)
      ).subscribe(
        result => {
          const links = super.resolveResult(result);
          if (links) {
            links['data'].map((link: ILink) => {
              link.texts.map(text => {
                text.text_paragraphs = JSON.parse(text.text_paragraphs);
              })
            });
            this.lns.initPaginator(this.paginator, links);
            this.subTableData$.next(links['data']);
          }
          this.isLoading = false;
        });
    }
  }

}

export class ContentDataSource extends DataSource<any> {
  constructor(private data$: Observable<ILink[]>) {
    super();
  }

  connect(): Observable<ILink[]> {
    return this.data$;
  }

  // updateTabel(index: number) {
  //   this.kupci$.subscribe(data => {
  //     data[index].isActive === 0 ? data[index].isActive = 1 : data[index].isActive = 0;
  //   });
  // }

  disconnect() { }
}