import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { FormControl } from '@angular/forms';

import { distinctUntilChanged, debounceTime, map } from 'rxjs/operators';
import { MatOptionSelectionChange } from '@angular/material';
import { IMenu } from 'src/app/cms/shared/models/menu/menu';
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-menusearch',
  template: `
  <mat-form-field class="col-12">
      <input type="text" placeholder="Unesite naziv teksta" aria-label="Number" matInput [formControl]="menu"
          [matAutocomplete]="auto">
          <mat-autocomplete #auto="matAutocomplete">
          <mat-option *ngFor="let option of filteredMenus$ | async; let i = index" [value]="option?.menu_name"
          (onSelectionChange)="onMenuSelected($event, option)">
            {{option?.menu_name}}
          </mat-option>
        </mat-autocomplete>
    </mat-form-field>
    <mat-progress-bar mode="indeterminate" *ngIf="isLoading"></mat-progress-bar>
    `,
  styles: []
})
export class MenusearchComponent implements OnInit {

  // @Input() txt_type: string
  @Output() mn: EventEmitter<IMenu[]> = new EventEmitter();

  subscription: Subscription;
  menu: FormControl = new FormControl('');
  selected: IMenu;
  isLoading = false;
  filteredMenus$: Observable<IMenu[]>;

  constructor(private mns: MenuService) { }

  ngOnInit() {
    this.subscription = this.menu.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(1000),
        map(menu => this._filter(menu))
      )
      .subscribe();
  }

  onMenuSelected(event: MatOptionSelectionChange, menu: IMenu) {
    if (event && event.isUserInput) {
      this.selected = menu;
      this.mn.emit([this.selected]);
    }
  }

  private _filter(value: string): IMenu[] {
    const filterValue = value.toLowerCase().trim();
    console.log(filterValue);
    const query = !this.selected || this.selected.menu_name.toLowerCase().trim() !== filterValue;
    if (query) {
      const filtered = [];
      if (filterValue && filterValue !== '') {
        this.isLoading = true;
        const search = [{ 'menu_name': filterValue }];
        this.filteredMenus$ = this.mns.getData(true, search).pipe(map(data => {
          if (data['data']) { this.isLoading = false; return data['data']; } else { this.isLoading = false; return []; }
        })
        );
      } else { this.isLoading = false; return filtered; }
    }
  }

  resetSrch() {
    this.menu.setValue('');
    this.selected = null;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
