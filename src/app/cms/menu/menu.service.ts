import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ICrudService } from '../shared/models/interfaces/common';
import { RequestService } from 'src/app/shared/services/request.service';
import { APPURL } from 'src/app/shared/globals';
import { map } from 'rxjs/operators';
import { IMenu } from '../shared/models/menu/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService implements ICrudService {
  constructor(private rs: RequestService) { }
  newData(dataObject: any): Observable<any> {
    return this.rs.post(APPURL + '/menu', dataObject).pipe(
      map(data => <IMenu>data)
    );
  }
  updateData(dataObject: any, id: number): Observable<any> {
    return this.rs.put(APPURL + '/menu/' + id, dataObject).pipe(
      map((Menu: IMenu) => Menu)
    );
  }
  getData(pagination: boolean, filter?: any): Observable<any> {
    return this.rs.get(APPURL + '/menu').pipe(
      map(data => <IMenu[]>data['data']));
  }
  paginatorUpdate(data: any) {
    this.rs.setPaginatorData(data);
  }
  initPaginator(paginator: import("@angular/material").MatPaginator, data: any) {
    this.rs.setPaginator(paginator, data);
  }


}


