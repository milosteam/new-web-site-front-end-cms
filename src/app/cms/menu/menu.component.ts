import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { IBaseFunctionality, IBasePagination, IBaseTab } from '../shared/models/interfaces/common';
import { FlatTreeControl, TreeControl } from '@angular/cdk/tree';

import { FormGroupDirective, FormBuilder, Validators } from '@angular/forms';
import { PageEvent, MatTabGroup, MatTreeFlattener, MatTreeFlatDataSource, MatSnackBar, MatTreeNode, MatTree, MatPaginator } from '@angular/material';
import { ArrayDataSource, DataSource } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag, CdkDropList } from '@angular/cdk/drag-drop';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { ILink, IFlatLink } from '../shared/models/links/links';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { MenuService } from './menu.service';
import { MenutreeService } from './menutree.service';
import { LinkService } from './link/link.service';
import { map } from 'rxjs/operators';
import { IfStmt } from '@angular/compiler';
import { IMenu } from '../shared/models/menu/menu';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent extends BasecomponentComponent
  implements OnInit, AfterViewInit, IBaseFunctionality, IBasePagination, IBaseTab {

  @ViewChild('menuTab', { static: true }) tabGroup: MatTabGroup;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild('menuTree', { static: false }) tree: MatTree<any>;


  menudata: ILink[] = [];
  links: ILink[] = [];
  updateModel: IMenu = null;
  updateLinksIds: number[];

  selectedTab: number;
  option: string;
  isLoading: boolean;

  // @ViewChild('textTab', { static: true }) tabGroup: MatTabGroup;

  treeControl: FlatTreeControl<IFlatLink>;
  treeFlattener: MatTreeFlattener<any, any>;
  dataSource: MatTreeFlatDataSource<ILink, IFlatLink>;
  expandedNodeSet = new Set<number | string>();

  starContainerId: string;
  dragging = false;
  expandTimeout: any;
  expandDelay = 1000;


  newMenu = this.fb.group({
    menu_name: ['', Validators.required]
  });

  //  Data Table
  subTableData$ = new BehaviorSubject([]);
  tableData$ = new ContentDataSource(this.subTableData$.asObservable());
  headerColumns = ['position', 'menu_name', 'link_count', 'edit'];
  //

  constructor(
    protected fus: FileuploadService,
    private route: ActivatedRoute,
    protected snackbar: MatSnackBar,
    private mns: MenuService,
    private trs: MenutreeService,
    private lns: LinkService,
    private fb: FormBuilder
  ) {
    super(fus, snackbar)
    this.treeFlattener = new MatTreeFlattener(this.trs.transformer, this.trs.getLevel, this.trs.isExpandable, node => node.children);
    this.treeControl = new FlatTreeControl<IFlatLink>(this.trs.getLevel, this.trs.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  }

  ngOnInit() {
    this.lns.getData(false).pipe(
      map(data => data['data'])).
      subscribe(links => this.links = links);
    this.trs.setTreeData([]);
  }

  ngAfterViewInit(): void {
    this.onTabSelect();
    this.trs.treeData$.subscribe(data => { this.dataSource.data = data; });
  }


  hasChild = (_: number, node: IFlatLink) => node.expandable;

  onFormSubmit(formGroup?: FormGroupDirective): void {
    if (this.newMenu.valid && this.newMenu.dirty && this.treeControl.dataNodes.length > 0) {
      this.isLoading = true;
      for (let i = this.treeControl.dataNodes.length - 1; i >= 0; i--) {
        if (this.treeControl.dataNodes[i].id_link <= 0) {
          this.treeControl.dataNodes.splice(i, 1);
        }
      }
      const menu = { menu_name: this.newMenu.get('menu_name').value, links: this.treeControl.dataNodes }
      this.onResetFrom();
      this.mns.newData(menu).subscribe(result => super.resolveResult(result, 'Uspešno ste kreirali novi meni.'));
    }
  }

  onFormUpdate(formGroup?: FormGroupDirective): void {

  }

  onResetFrom(formGroup?: FormGroupDirective): void {
    this.lns.getData(false).pipe(
      map(data => data['data'])).
      subscribe(links => this.links = links);
    this.newMenu.reset();
    formGroup ? formGroup.reset() : false;
    this.trs.setTreeData(this.menudata = []);
  }

  modelForUpdate(model: any) {
    this.updateModel = model;
    this.rebuildMenuData(this.updateModel.links);
    this.filterLinkLinst();
    this.selectTab(this.option = 'novi');
  }


  onPaginationChange(event: PageEvent): void {

  }

  selectTab(tab: string): void {
    if (tab === 'novi') {
      this.tabGroup.selectedIndex = 0;
    }
    // tslint:disable-next-line:one-line
    else if (tab === 'list') {
      if (this.newMenu.dirty) {
        this.onResetFrom();
      }
      this.tabGroup.selectedIndex = 1;
    }
    // tslint:disable-next-line:one-line
    else { this.selectedTab = 0; }
  }

  selSelectedTab(): void {
    this.route.params.subscribe(
      data => {
        if (data['option']) {
          this.option = data['option'];
          this.selectTab(this.option);
        }
      });
  }

  onTabSelect(): void {
    this.tabGroup.selectedIndexChange.subscribe(data => {
      if (data === 1) {
        this.option = 'list'; this.selectTab(this.option);
        this.onResetDataTable();
      }
      if (data === 0) { this.option = 'novi'; this.selectTab(this.option); }
    });
  }


  onResetDataTable(first = true) {
    this.isLoading = true; // Progress bar trigger
    if (!super.firstPagePagination(this.paginator, first)) { // List reload on selected page
      this.mns.getData(true).subscribe(
        result => {
          console.log(result);
          const menus = super.resolveResult(result);
          console.log(menus);
          if (menus) {
            this.lns.initPaginator(this.paginator, menus);
            this.subTableData$.next(menus['data']);
          }
          this.isLoading = false;
        });
    }
  }



  filterLinkLinst() {
    if (this.updateModel) {
       this.links = this.links.filter(link => !this.updateLinksIds.includes(link.id_link));
    }
  }

  onMouseDown(node) {
    if (this.treeControl.isExpanded(node)) {
      this.treeControl.collapse(node);
      this.expandedNodeSet.has(node.id_link) ? this.expandedNodeSet.delete(node.id_link) : false;

    };
  }

  onExpandClick(node) {
    if (this.treeControl.isExpanded(node)) {
      this.treeControl.collapse(node);
      this.expandedNodeSet.has(node.id_link) ? this.expandedNodeSet.delete(node.id_link) : false;
    }
    else { this.treeControl.expand(node) };
  }

  dragStart(event: { source: CdkDrag }, node) {
    this.starContainerId = event.source.dropContainer.id;
    if (this.starContainerId === 'menuList' && this.treeControl.isExpanded(node)) {
      this.treeControl.collapse(node);

    }
    this.dragging = true;
  }

  dragEnd() {
    this.dragging = false;
  }

  private rebuildMenuData(dbLinks: ILink[]) {
    if (dbLinks.length <= 0) return;
    this.updateLinksIds = [];
    dbLinks.reduceRight((acc, value, index) => {
      this.trs.initLinkInMenu(value, true, value.pivot.level, value.pivot.position, value.pivot.id_parent_link);
      if (value.pivot.id_parent_link === 0) {
        this.updateLinksIds.push(value.id_link);
        this.menudata[value.pivot.position] = dbLinks.splice(index, 1)[0]; return [];
      }
      else {
        dbLinks.map(link => {
          if (link.id_link === value.pivot.id_parent_link) {
            this.updateLinksIds.push(value.id_link);
            !link.children ? this.trs.initLinkInMenu(link) : false;
            link.children[value.pivot.position] = dbLinks.splice(index, 1)[0];
          }
        });
      }
    }, []);
    this.trs.setTreeData(this.menudata);
  }

  private rememberExpandedTreeNodes(
    treeControl: FlatTreeControl<IFlatLink>,
    expandedNodeSet: Set<string>
  ) {
    this.expandedNodeSet.clear();
    if (treeControl.dataNodes) {
      treeControl.dataNodes.forEach((node) => {
        if (treeControl.isExpandable(node) && treeControl.isExpanded(node)) {
          // capture latest expanded state
          expandedNodeSet.add(node.domId);
        }
      });
    }
  }


  expandParents() {
    this.treeControl.dataNodes.map(node => {
      if (node.id_link && this.expandedNodeSet.has(node.id_link)) {
        this.treeControl.expand(node);
      }
    });
  }


  findNewPosition(saveTreeData: FlatTreeControl<IFlatLink>, curentIndex: number, inout: number) {
    let counter = 0; // Counter of nodes that are courently visible in the tree state at the moment of drop event.
    this.trs.rememberExpandedTreeNodes(saveTreeData, this.expandedNodeSet); // Collecting expanded node ids in the tree.
    console.log(this.expandedNodeSet);
    for (const node in saveTreeData.dataNodes) {
      const parenId = (<IFlatLink>saveTreeData.dataNodes[node]).id_parent_link;
      if (parenId === 0) counter++; // Counting nodes that are in the first row
      if (parenId !== 0 && this.expandedNodeSet.has(parenId)) counter++; // Counting nodes that are children of expaneded parents.
      if (counter === (curentIndex + 1)) {
        if (saveTreeData.isExpanded(saveTreeData.dataNodes[node]) && inout <= 0) {
          return saveTreeData.dataNodes[parseInt(node) + 1];
        } else {
          return saveTreeData.dataNodes[node];
        }
      } // Returning previus node at the desired position
    }
  }

  setNodeInPosition(preLink: IFlatLink, newLink: IFlatLink, updown) {
    if (!preLink || preLink === newLink) return; // If picked and drop at the same position do nothing
    if (preLink.id_link < 0) updown = 1;  // If previus link is ADDNEW FAKE LINK don't allow new link to be placed under it.
    console.log(updown);
    const oldpath = []; this.findNodePathInArray(newLink.id_link, this.menudata, oldpath);
    const node = this.removeNodeByPath(this.menudata, oldpath);
    const newpath = []; this.findNodePathInArray(preLink.id_link, this.menudata, newpath);
    if (JSON.stringify(oldpath) === JSON.stringify(newpath) && updown <= 0) { newpath[0] = (parseInt(newpath[0]) + 1).toString(); } // Using updown we determan if the item goes before or after current item in position
    this.setNodeData(newpath.length - 1, preLink.id_parent_link, <ILink>node);
    this.insertNodeByNewPath(this.menudata, newpath, <ILink>node);
  }

  /*Returns array with indexes from the provided node and parents back to the first level in array */
  findNodePathInArray(idLink: number | ILink, arr: ILink[], result: string[], ) {
    for (let index in arr) {
      if (arr[index].id_link === idLink) {
        result.push(index); if (arr[index].inmenu.id_parent_link === 0) break;
        else {
          this.findNodePathInArray(arr[index].inmenu.id_parent_link, this.menudata, result);
        }
      }
      else {
        this.findNodePathInArray(idLink, arr[index].children, result);
      }
    }
  }

  removeNodeByPath(array: ILink[], path: Number[]): ILink | string {
    const link = path.reduceRight((acc, value, index) => {
      if (path.length === 1) {
        const node = eval(acc).splice(value, 1)[0];
        this.recalculatePositions(eval(acc));
        return node;
      } // If node is parent or in the first array level remove it.
      else if (index === path.length - 1) { return acc + '[' + value + ']'; } // Setting parent object in the first lvl.
      else if (index > 0) return acc + '.children[' + value + ']'; // Setting every other parent in children arrays of nested nodes.
      else {
        const node = eval(acc + '.children').splice(value, 1)[0];
        this.recalculatePositions(eval(acc + '.children'));
        return node;
      } // Splice node from the last parent children array.
    }, 'array');
    return link;
  }

  private recalculatePositions(arr: ILink[]) {
    console.log(arr);
    arr.map((link, index) => {
      link.inmenu.position = index;
    });
  }

  insertNodeByNewPath(array: ILink[], path: Number[], node: ILink): ILink | string {
    const link = path.reduceRight((acc, value, index) => {
      console.log(value);
      if (path.length === 1) {
        const link = eval(acc).splice(value, 0, node);
        this.recalculatePositions(eval(acc));
        return link;
      } // If node is parent or in the first array level remove it.
      else if (index === path.length - 1) { return acc + '[' + value + ']'; } // Setting parent object in the first lvl.
      else if (index > 0) return acc + '.children[' + value + ']'; // Setting every other parent in children arrays of nested nodes.
      else {
        const link = eval(acc + '.children').splice(value, 0, node);
        this.recalculatePositions(eval(acc + '.children'));
        return link;
      } // Splice node from the last parent children array.
    }, 'array');
    return link;
  }

  setNodeData(newLevel: number, parentId: number, newNode: ILink) {
    newNode.inmenu.row = newLevel;
    newNode.inmenu.id_parent_link = parentId;
    if (newNode.children && newNode.children.length > 0) {
      newNode.children.map(node => {
        this.setNodeData((newNode.inmenu.row + 1), newNode.id_link, node);
      });
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    if (!event.isPointerOverContainer) return;
    const changedData = Object.assign({}, this.treeControl.dataNodes);
    const node = event.item.data;
    if (event.previousContainer !== event.container) {
      this.trs.moveListMenu(event, this.links, this.menudata);
      this.trs.setTreeData(this.menudata);
    } else if (event.previousContainer === event.container && event.container.id === 'menuList') {
      const direction = event.previousIndex - event.currentIndex;
      const currentNode = this.findNewPosition(this.treeControl, event.currentIndex, direction);
      console.log(currentNode);
      this.setNodeInPosition(currentNode, node, direction);
      this.trs.setTreeData(this.menudata);
      this.expandParents();
    }
    else {
      alert('Ne može');
    }
  }

}

export class ContentDataSource extends DataSource<any> {
  constructor(private data$: Observable<IMenu[]>) {
    super();
  }

  connect(): Observable<IMenu[]> {
    return this.data$;
  }

  // updateTabel(index: number) {
  //   this.kupci$.subscribe(data => {
  //     data[index].isActive === 0 ? data[index].isActive = 1 : data[index].isActive = 0;
  //   });
  // }

  disconnect() { }
}




