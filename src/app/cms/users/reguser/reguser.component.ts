import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';

import { USERROLE, DEPARTMENT, USERROLES, IUserinfo } from '../../../shared/models/user';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher, MatSnackBar, MatTabGroup, MatPaginator, PageEvent } from '@angular/material';
import { ReuserService } from './reuser.service';
import { DataSource } from '@angular/cdk/table';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';

@Component({
  selector: 'app-reguser',
  templateUrl: './reguser.component.html',
  styleUrls: ['./reguser.component.scss']
})
export class ReguserComponent extends BasecomponentComponent implements OnInit, AfterViewInit {

  @ViewChild('userTab', { static: true }) userTab: MatTabGroup;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  private option: string;
  private selectedTab = 1;

  isLoading = false;

  private passwordsMatcher = new RepeatPasswordEStateMatcher;
  displayedColumns2: string[] = ['rbroj', 'username', 'datecreated', 'phone', 'dept', 'izmeni'];

  private subusers$: BehaviorSubject<IUserinfo[]> = new BehaviorSubject([]);
  users$: UserDataSource = new UserDataSource(this.subusers$.asObservable());

  ovlascenja = USERROLE;
  sektor = DEPARTMENT;

  updateUser: IUserinfo = null;

  newUser: FormGroup = this.fb.group({
    user: this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirmpassword: ['', Validators.required]
    }, { validator: this.checkPasswords }),
    userinfo: this.fb.group({
      userRole: ['', Validators.required],
      department: [''],
      username: [''],
      phone: ['']
    })
  });


  constructor(
    private fb: FormBuilder,
    private rus: ReuserService,
    protected snackBar: MatSnackBar,
    private router: Router,
    protected fus: FileuploadService,
  ) { super(fus, snackBar); }

  ngOnInit() { }

  ngAfterViewInit(): void {
    this.userTab.selectedIndexChange.subscribe(data => {
      if (data === 1) {
        this.option = 'list';
        this.updateUser = null;
        this.selectTab(this.option);
      }
    });
  }

  private checkPasswords(group: FormGroup) {
    const pass = group.controls.password.value;
    const confirm = group.controls.confirmpassword.value;
    return pass === confirm ? null : { passwordsNotEqual: true };
  }

  newUserSubmit() {
    if (!this.newUser.pristine && this.newUser.valid) {
      this.isLoading = true;
      this.rus.regApiUser(this.newUser.value).subscribe(data => {
        super.resolveResult(data, 'Uspešno ste kreirali novog korisnika.');
        this.isLoading = false;
      });
    }
  }

  onResetUserList() {
    this.isLoading = true;
    this.rus.getRegUsers().subscribe(
      data => {
        const users = super.resolveResult(data);
        if (users) {
          this.rus.initKupacPaginator(this.paginator, users);
          this.subusers$.next(users['data']);
        }
        this.isLoading = false;
      }
    );
  }

  onResetPass(element: IUserinfo) {
    this.router.navigate(['./respass'], { queryParams: { order: 'popular' } });
  }

  selectTab(tab: string): void {
    if (tab === 'novi') {
      this.userTab.selectedIndex = 0;
    }
    // tslint:disable-next-line:one-line
    else if (tab === 'list') {
      this.onResetUserList();
      this.resetUserForm();
      this.userTab.selectedIndex = 1;
    }
    // tslint:disable-next-line:one-line
    else { this.selectedTab = 0; }
  }

  onUserEdit(user: IUserinfo) {
    this.updateUser = user;
    this.populateUserForm();
    this.selectTab('novi');
  }

  populateUserForm() {
    // tslint:disable-next-line:forin
    for (const role in USERROLES) {
      if (USERROLES[role] === this.updateUser.userRole) {
        (<FormGroup>this.newUser.controls['userinfo']).controls['userRole'].setValue(role); break;
      }
    }
    (<FormGroup>this.newUser.controls['userinfo']).controls['username'].setValue(this.updateUser.username);
    (<FormGroup>this.newUser.controls['userinfo']).controls['phone'].setValue(this.updateUser.phone);
    (<FormGroup>this.newUser.controls['userinfo']).controls['department'].setValue(this.updateUser.department);
  }

  onUpdateUser() {
    if (this.newUser.controls['userinfo'].valid) {
      this.isLoading = true;
      this.rus.updateUserInfo(this.newUser.controls['userinfo'].value, this.updateUser.idUserInfo).subscribe(data => {
        super.resolveResult(data, 'Uspešno ste izmenili korisničke podatke.');
        this.isLoading = false;
      });
    }
  }

  onPaginationChange(event: PageEvent) {
    if (event) {
      this.rus.paginatorUpdate(event);
      this.onResetUserList();
    }
  }

  resetUserForm() {
    this.newUser.reset();
    this.updateUser = null;
  }

}

export class RepeatPasswordEStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return (control && control.parent.get('password').value !== control.parent.get('confirmpassword').value && control.dirty);
  }
}

export class UserDataSource extends DataSource<any> {
  constructor(private users$: Observable<IUserinfo[]>) {
    super();
  }

  connect(): Observable<IUserinfo[]> {
    return this.users$;
  }

  disconnect() { }
}
