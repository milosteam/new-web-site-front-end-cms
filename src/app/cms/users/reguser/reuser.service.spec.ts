import { TestBed, inject } from '@angular/core/testing';

import { ReuserService } from './reuser.service';

describe('ReuserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ReuserService]
    });
  });

  it('should be created', inject([ReuserService], (service: ReuserService) => {
    expect(service).toBeTruthy();
  }));
});
