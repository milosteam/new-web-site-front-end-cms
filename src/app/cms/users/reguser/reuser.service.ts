import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { map, tap } from 'rxjs/operators';
import {RequestService} from '../../../shared/services/request.service';
import { APPURL} from '../../../shared/globals';
import { PageEvent, MatPaginator } from '@angular/material';
import { IUserinfo, IUser } from 'src/app/shared/models/user';


@Injectable({
  providedIn: 'root'
})
export class ReuserService {

  constructor(private http: RequestService) { }

public regApiUser(userdata: any) {
  return this.http.post(APPURL +  '/regapiuser', userdata).pipe(
    map(data => data)
  );
}

public getRegUsers() {
  return this.http.get(APPURL + '/users').pipe(
    map( data => data['data'])
  );
}

updateUserInfo(user: IUserinfo, id: number): Observable<IUserinfo> {
  return this.http.put(APPURL + '/updateuserinfo/' + id, user).pipe(
    map(data => <IUserinfo>data)
  );
}

//  Pagination **************************************

paginatorUpdate(data: any) {
  this.http.setPaginatorData(data);
}
initKupacPaginator(paginator: MatPaginator, data: any) {
  this.http.setPaginator(paginator, data);
}

//  *******************************************************

}
