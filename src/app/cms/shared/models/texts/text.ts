import { ILanguage } from "../langauge/langauge";

export interface Iparagraph {
    paragraph_name?:string;
    paragraph_text:string;
}

export interface IParagraphArray {
    [index: number]: Iparagraph;
}

export interface IText {
    readonly id_text: number;
    text_type: string;
    text_name?: string;
    text_title?: string;
    text_paragraphs: string; 
    paragraphs?: IParagraphArray;
    id_language: number;
    language?: ILanguage;
    parent: string;

}

export const ObjParagraph = {
    title_par: { value: '', disabled: false },
    text: { value: '', disabled: false },
  };

  export const ObjLineText = {
    id_language: {value: '', disabled: false},  
    text_type: {value: 'line', disabled:false },
    title_par: { value: '', disabled: false },
    text: { value: '', disabled: false },
  };

export const TEXTTYPE = [
    'line',
    'simple',
    'rich'
]

export const TEXT_PARENT = 'text';