export const PARENT = 'language';

export interface ILanguage {
readonly id_language?: number;
language: string;
identifier: string;
parent: string;
}