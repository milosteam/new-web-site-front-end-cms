import { ILink, IFlatLink } from "../links/links";

export const MENU_PARENT = 'menu';

// export class MenuNode {
//     name: string;
//     children?: MenuNode[];
//   }

export interface IMenu {
    readonly id_menu?: number;
    menu_name: string;
    menu_type: number;
    is_active: number;
    links?: [];
}