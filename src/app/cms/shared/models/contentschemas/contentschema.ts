export interface IContenshcema {
   readonly id_content_schema?: number,
   schema_name: string,
   schema_type: number,
   is_sub_schema: number,
   created_at?: Date,
   updated_at?: Date,
}
export const CONTENTSCHEMA_TYPE = ['page', 'section'];

export const BASIC_TYPES = {
   image: ['seo'],
   text: ['line', 'simple', 'rich'],
   link: ['simple'],
   menu: ['classic']
}

