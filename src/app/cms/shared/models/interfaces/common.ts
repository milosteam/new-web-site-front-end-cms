import { FormGroupDirective } from "@angular/forms";
import { Observable, BehaviorSubject } from "rxjs";
import { PageEvent, MatTabGroup, MatPaginator } from "@angular/material";
import { ActivatedRoute } from "@angular/router";
import { DataSource } from "@angular/cdk/table";

export interface IBaseFunctionality {
    updateModel: any;
    onFormSubmit(formGroup?: FormGroupDirective): void;
    onFormUpdate(formGroup?: FormGroupDirective): void;
    onResetFrom(formGroup?: FormGroupDirective): void;
    modelForUpdate(model: any): any;
}

export interface IBasePagination {
    onPaginationChange(event: PageEvent): void; 
}

export interface IBaseTab {
    tabGroup: MatTabGroup;
    selectedTab: number;
    option: string;
    selectTab(tab: string): void;
    selSelectedTab(): void;
    onTabSelect(): void;
}

export interface ICrudService {
    newData(dataObject: any): Observable<any>;
    updateData(dataObject: any, id: number): Observable<any>;
    getData(pagination: boolean, filter?: any): Observable<any>;
    paginatorUpdate(data: any);
    initPaginator(paginator: MatPaginator, data: any);
}

export interface IDataTable {
    subTableData$: BehaviorSubject<any>;
    tableData$: DataSource<any>;
    headerColumns: string[];
    onResetDataTable(first?: boolean);
}
