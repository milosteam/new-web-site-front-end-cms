import { IText, IParagraphArray } from "../texts/text";
import { ILanguage } from "../langauge/langauge";

interface ILinkInMenu {
    readonly id_menu?: number;
    id_link?: number;
    id_parent_link?: number;
    row?: number;
    position?: number;
    isParent?: boolean;
    domId?: string;
    expandable?: boolean;
}


export interface ILink {
    readonly id_link?: number;
    link_name: string;
    link_description?: string;
    link_auth?: number;
    paragraphs?: IText[];
    texts?: IText[];
    languages?: number[];
    children?: ILink[];
    index?: number;
    inmenu?: ILinkInMenu;
    parent: string;
    pivot?: IFlatLink;
}

export interface IFlatLink {
    id_link?: number;
    link_name: string;
    id_parent_link?: number;
    level?: number;
    position?: number;
    domId?: string;
    expandable?: boolean;
    isParent?: boolean;
}


export const LINK_PARENT = 'link';