export interface IImageValidator {
    allowedExt?: string[];
    maxSize?: number;
    minSize?: number;
    numOfImages?: number;
}