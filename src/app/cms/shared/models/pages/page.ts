export interface IPage {
readonly id_page?: number;
page_name: string;
page_visited?: number;
id_page_seo?: number;
id_social?: number;
id_link?: number;
id_page_content?: number;
id_user?: number;
active?: number;
}

export interface IPagecontents {
readonly id_page_content?: number;
content_name: string;
page_template_name: string;
id_content_schema?: number;
}