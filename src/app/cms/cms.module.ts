import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CmsRoutingModule } from './cms-routing.module';
import { CmslandingComponent } from './cmslanding/cmslanding.component';
import { SharedModule } from '../shared/shared.module';

import { UsersComponent } from './users/users.component';
import { ReguserComponent } from './users/reguser/reguser.component';
import { ContentschemasComponent } from './contentschemas/contentschemas.component';
import { PageComponent } from './page/page.component';
import { TextsComponent } from './texts/texts.component';
import { SettingsComponent } from './settings/settings.component';
import { LanguagesComponent } from './settings/languages/languages.component';
import { MenuComponent } from './menu/menu.component';
import { LinkComponent } from './menu/link/link.component';
import { TextsearchComponent } from './texts/textsearch/textsearch/textsearch.component';
import { ImagesComponent } from './images/images.component';
import { MenusearchComponent } from './menu/menusearch/menusearch.component';


@NgModule({
  declarations: [CmslandingComponent, UsersComponent, ReguserComponent, ContentschemasComponent, PageComponent, TextsComponent, SettingsComponent, LanguagesComponent, MenuComponent, LinkComponent, TextsearchComponent, ImagesComponent, MenusearchComponent],
  imports: [
    CommonModule,
    CmsRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class CmsModule { }
