import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray, FormGroupDirective } from '@angular/forms';
import { NumberValidator } from '../shared/validators/validators';
import { BASIC_TYPES, CONTENTSCHEMA_TYPE, IContenshcema } from '../shared/models/contentschemas/contentschema';
import { IBaseFunctionality, IBaseTab, IDataTable } from '../shared/models/interfaces/common';
import { MatTabGroup, MatPaginator, MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ContentschemaService } from './contentschema.service';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { DataSource } from '@angular/cdk/table';
import { TextsearchComponent } from '../texts/textsearch/textsearch/textsearch.component';
import { MenusearchComponent} from '../menu/menusearch/menusearch.component';

@Component({
  selector: 'app-contentschemas',
  templateUrl: './contentschemas.component.html',
  styleUrls: ['./contentschemas.component.scss']
})
export class ContentschemasComponent extends BasecomponentComponent
  implements OnInit, AfterViewInit, IBaseFunctionality, IBaseTab, IDataTable {


  //  Data Table
  subTableData$ = new BehaviorSubject([]);
  tableData$ = new ContentDataSource(this.subTableData$.asObservable());
  headerColumns = ['position', 'name', 'type', 'edit'];
  updateModel: IContenshcema;
  //

  BASIC_TYPES = BASIC_TYPES;
  CONTENTSCHEMA_TYPE = CONTENTSCHEMA_TYPE;
  isLoading = false;


  @ViewChild('kupacTab', { static: true }) tabGroup: MatTabGroup;
  selectedTab = 1;
  option: string;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  objModel = {
    model_name: { value: '', disabled: false },
    model_type: { value: '', disabled: false },
    id_model: { value: 0, disabled: false }
  };

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private css: ContentschemaService,
    protected snackBar: MatSnackBar,
    protected fus: FileuploadService
  ) {
    super(fus, snackBar);
  }
  modelForUpdate(model: any) {
    throw new Error("Method not implemented.");
  }

  newSchema = this.fb.group({
    schema_name: ['', Validators.required],
    schema_type: ['', [Validators.required]],
    is_complex_schema: [false, [Validators.required]],
    seoimage: this.fb.array([]),
    linetext: this.fb.array([]),
    simpletext: this.fb.array([]),
    richtext: this.fb.array([]),
    classicmenu: this.fb.array([]),
    simplelink: this.fb.array([])
  });

  ngOnInit(): void {
    this.selSelectedTab();
    console.log(this.newSchema);
  }

  ngAfterViewInit(): void {
    this.onTabSelect();
  }


  selectTab(tab: string): void {
    if (tab === 'novi') {
      this.tabGroup.selectedIndex = 0;
    }
    // tslint:disable-next-line:one-line
    else if (tab === 'list') {
      if (this.newSchema.dirty) {
        this.onResetFrom();
      }
      this.tabGroup.selectedIndex = 1;
    }
    // tslint:disable-next-line:one-line
    else { this.selectedTab = 0; }
  }

  selSelectedTab(): void {
    this.route.params.subscribe(
      data => {
        if (data['option']) {
          this.option = data['option'];
          this.selectTab(this.option);
        }
      });
  }

  onTabSelect(): void {
    this.tabGroup.selectedIndexChange.subscribe(data => {
      if (data === 1) {
        this.option = 'list'; this.selectTab(this.option);
        this.onResetDataTable();
      }
      if (data === 0) { this.option = 'novi'; this.selectTab(this.option); }
    });
  }

  onFormSubmit(formGroup?: FormGroupDirective) {
    if (!this.newSchema.pristine && this.newSchema.valid) {
      this.isLoading = true;
      console.log(this.newSchema.value);
      this.css.newData(<IContenshcema>this.newSchema.value).subscribe(
        data => {
          super.resolveResult(data, 'Uspešno ste dodali novu strukturnu šemu');
          this.onResetFrom();
          this.isLoading = false;
        }
      );
    }
  }

  onFormUpdate(formGroup?: FormGroupDirective) {

  }

  onResetFrom(formGroup?: FormGroupDirective) {
    Object.keys(this.newSchema.controls).map(key =>
      this.newSchema.get(key) instanceof FormArray && (<FormArray>this.newSchema.get(key)).length > 0 ? console.log(key) : false)
    this.newSchema.reset();
    formGroup ? formGroup.reset() : false;
  }

  onSchemaUpdate(schema: IContenshcema) {
    this.updateModel = schema;
    super.objectToForm(this.newSchema, this.updateModel);
    this.newSchema.markAsDirty();
    this.option = 'novi';
    this.selectTab(this.option);
  }

  onResetDataTable(first = true) {
    this.isLoading = true; // Progress bar trigger
    if (!super.firstPagePagination(this.paginator, first)) { // List reload on selected page
      this.css.getData(true).subscribe(
        data => {
          console.log(data);
          const contentschemas = super.resolveResult(data);
          if (contentschemas) {
            console.log(contentschemas);
            this.css.initPaginator(this.paginator, contentschemas);
            this.subTableData$.next(contentschemas['data']);
          }
          this.isLoading = false;
        });
    }
  }

  addToSchema(item: string, value: any) {
    const control = `${value}${item}`;
    this.objModel.model_name.value = item;
    this.objModel.model_type.value = value;
    super.addToFormArray(this.objModel, this.newSchema.get(control));
    console.log(this.newSchema.get(control));
  }

  onPaginationChange(event: import("@angular/material").PageEvent): void {
    if (event) {
      this.css.paginatorUpdate(event);
      this.onResetDataTable();
    }
  }

}

export class ContentDataSource extends DataSource<any> {
  constructor(private kupci$: Observable<IContenshcema[]>) {
    super();
  }

  connect(): Observable<IContenshcema[]> {
    return this.kupci$;
  }

  // updateTabel(index: number) {
  //   this.kupci$.subscribe(data => {
  //     data[index].isActive === 0 ? data[index].isActive = 1 : data[index].isActive = 0;
  //   });
  // }

  disconnect() { }
}


