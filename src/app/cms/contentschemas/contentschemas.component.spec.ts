import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentschemasComponent } from './contentschemas.component';

describe('ContentschemasComponent', () => {
  let component: ContentschemasComponent;
  let fixture: ComponentFixture<ContentschemasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentschemasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentschemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
