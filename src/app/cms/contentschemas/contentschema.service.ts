import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/shared/services/request.service';
import { ICrudService } from '../shared/models/interfaces/common';
import { Observable } from 'rxjs';
import { MatPaginator } from '@angular/material';
import { APPURL } from 'src/app/shared/globals';
import { map } from 'rxjs/operators';
import { IContenshcema } from '../shared/models/contentschemas/contentschema';

@Injectable({
  providedIn: 'root'
})
export class ContentschemaService implements ICrudService {


  constructor(private rs: RequestService) { }
 
  newData(dataObject: any): Observable<any> {
    return this.rs.post(APPURL + '/contenschema', dataObject).pipe(
      map(data => <IContenshcema>data)
    );
  }

  updateData(dataObject: any, id: number): Observable<any> {
    return null;
  }
  
  getData(pagination: boolean, filter?: any): Observable<any> {
    return this.rs.get(APPURL + '/contenschema').pipe(
      map(data => <IContenshcema[]>data['data']));
  }

  paginatorUpdate(data: any) {
    this.rs.setPaginatorData(data);
  }

  initPaginator(paginator: MatPaginator, data: any) {
    this.rs.setPaginator(paginator, data);
  }
}
