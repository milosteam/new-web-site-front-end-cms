import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { IBaseFunctionality, IBasePagination, IBaseTab } from '../shared/models/interfaces/common';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { MatSnackBar } from '@angular/material';
import { TextService } from '../texts/text.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent extends BasecomponentComponent
implements OnInit, AfterViewInit, IBaseFunctionality, IBasePagination, IBaseTab {


  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    protected fus: FileuploadService,
    protected snackBar: MatSnackBar,
    private txs: TextService
   ) {super(fus, snackBar); }
  modelForUpdate(model: any) {
    throw new Error("Method not implemented.");
  }


  ngAfterViewInit(): void {
    throw new Error("Method not implemented.");
  }
  updateModel: any;
  onFormSubmit(formGroup?: import("@angular/forms").FormGroupDirective): void {
    throw new Error("Method not implemented.");
  }
  onFormUpdate(formGroup?: import("@angular/forms").FormGroupDirective): void {
    throw new Error("Method not implemented.");
  }
  onResetFrom(formGroup?: import("@angular/forms").FormGroupDirective): void {
    throw new Error("Method not implemented.");
  }
  onPaginationChange(event: import("@angular/material").PageEvent): void {
    throw new Error("Method not implemented.");
  }
  tabGroup: import("@angular/material").MatTabGroup;
  selectedTab: number;
  option: string;
  selectTab(tab: string): void {
    throw new Error("Method not implemented.");
  }
  selSelectedTab(): void {
    throw new Error("Method not implemented.");
  }
  onTabSelect(): void {
    throw new Error("Method not implemented.");
  }

 

  ngOnInit(): void {
  }

}
