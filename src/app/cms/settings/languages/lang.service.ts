import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/shared/services/request.service';

import { Observable } from 'rxjs';
import { MatPaginator } from '@angular/material';
import { APPURL } from 'src/app/shared/globals';
import { map } from 'rxjs/operators';
import { ICrudService } from '../../shared/models/interfaces/common';
import { IContenshcema } from '../../shared/models/contentschemas/contentschema';
import { ILanguage } from '../../shared/models/langauge/langauge';


@Injectable({
  providedIn: 'root'
})
export class LangService implements ICrudService {


  constructor(private rs: RequestService) { }
 
  newData(dataObject: any): Observable<any> {
    return this.rs.post(APPURL + '/lang', dataObject).pipe(
      map(data => <IContenshcema>data)
    );
  }

  updateData(dataObject: any, id: number): Observable<any> {
    return this.rs.put(APPURL + '/lang/' + id, dataObject).pipe(
      map((uredjaj : ILanguage) => uredjaj)
    );
  }
  
  getData(pagination: boolean, filter?: any): Observable<any> {
    return this.rs.get(APPURL + '/lang').pipe(
      map(data => <IContenshcema[]>data['data']));
  }

  paginatorUpdate(data: any) {
    this.rs.setPaginatorData(data);
  }

  initPaginator(paginator: MatPaginator, data: any) {
    this.rs.setPaginator(paginator, data);
  }
}
