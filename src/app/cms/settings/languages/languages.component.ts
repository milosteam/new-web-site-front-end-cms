import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { IBaseFunctionality, IBasePagination, IBaseTab } from '../../shared/models/interfaces/common';
import { FormBuilder, FormArray, Validators, FormGroupDirective } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { MatSnackBar, MatTabGroup, MatPaginator, PageEvent } from '@angular/material';
import { FileuploadComponent } from 'src/app/shared/components/fileupload/fileupload.component';

import { DataSource } from '@angular/cdk/table';
import { Observable, BehaviorSubject } from 'rxjs';
import { ILanguage, PARENT } from '../../shared/models/langauge/langauge';
import { LangService } from './lang.service';
import { ContentschemaService } from '../../contentschemas/contentschema.service';
import { FILEURL } from 'src/app/shared/globals';
import { IImageValidator } from '../../shared/models/images/image';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss']
})
export class LanguagesComponent extends BasecomponentComponent
  implements OnInit, AfterViewInit, IBaseFunctionality, IBasePagination, IBaseTab {

  PARENT = PARENT;
  FILEURL = FILEURL;

  @ViewChild('textTab', { static: true }) tabGroup: MatTabGroup;
  selectedTab = 1;
  option: string;

  @ViewChild('fuc', { static: false }) fuc: FileuploadComponent;
  imageSettings: IImageValidator = { numOfImages: 4, allowedExt: ['jpg', 'png', 'jpeg', 'gif'] }
  imageSrcs = [];


  newLangauge = this.fb.group({
    language: ['', [Validators.required]],
    identifier: ['', [Validators.required]],
    katImage: this.fb.array([
    ]),
    parent: [PARENT, [Validators.required]]
  });

  isLoading = false;

  //  Data Table
  subTableData$ = new BehaviorSubject([]);
  tableData$ = new ContentDataSource(this.subTableData$.asObservable());
  headerColumns = ['position', 'icon', 'name', 'type', 'edit'];
  updateModel: ILanguage;
  //

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    protected fus: FileuploadService,
    protected snackBar: MatSnackBar,
    private lgs: LangService
  ) { super(fus, snackBar); }

  ngOnInit(): void {
    this.selSelectedTab();
  }
  ngAfterViewInit(): void {
    this.onTabSelect();

    this.fuc.$exportFiles.subscribe(file => {
      if (file) {
        this.imageSrcs = [];
        super.readURL(this.imageSrcs, <FileList>file, this.newLangauge.get('katImage') as FormArray);
      } else {
        this.imageSrcs = [];
        (this.newLangauge.get('katImage') as FormArray).reset();
      }
    });


    this.fuc.setUploadSettings(this.imageSettings);
  }

  onFormSubmit(formGroup?: FormGroupDirective): void {
   if (this.newLangauge.valid && this.newLangauge.dirty) {
      this.isLoading = true;
      const DATA = super.jsonToFormData(this.newLangauge);
      this.lgs.newData(DATA).subscribe(
        result => {
          console.log(result);
          super.resolveResult(result, 'Uspešno ste dodali novi jezik.')
          this.isLoading = false;
          this.onResetFrom(formGroup);
        }
      )
    }
  }

  onFormUpdate(formGroup?: FormGroupDirective): void {
    if (this.newLangauge.valid && this.newLangauge.dirty) {
      this.isLoading = true;
      console.log(this.updateModel.id_language);
      this.lgs.updateData(this.newLangauge.value, this.updateModel.id_language).subscribe(
        result => {
          super.resolveResult(result, 'Uspešno ste izmenili jezik.');
          this.isLoading = false;
          this.onResetFrom(formGroup);
        }
      )
    }
  }

  modelForUpdate(language: ILanguage) {
    console.log(language);
    if (language) {
      this.updateModel = language;
      this.updateModel.parent = PARENT;
      super.objectToForm(this.newLangauge, this.updateModel);
      this.newLangauge.markAsDirty();
      this.option = 'novi';
      this.selectTab(this.option);
    }
  }


  onResetFrom(formGroup?: FormGroupDirective): void {
    this.updateModel ? this.updateModel = null : false;
    formGroup ? formGroup.reset() : false;
    this.newLangauge.reset();
    this.newLangauge.get('parent').setValue('language'); //Sets image type for backend.
    this.isLoading === true ? this.isLoading = false : false;
  }

  onPaginationChange(event: PageEvent): void {
    if (event) {
      this.lgs.paginatorUpdate(event);
      this.onResetDataTable();
    }
  }


  selectTab(tab: string): void {
    if (tab === 'novi') {
      this.tabGroup.selectedIndex = 0;
    }
    // tslint:disable-next-line:one-line
    else if (tab === 'list') {
      if (this.newLangauge.dirty) {
        this.onResetFrom();
      }
      this.tabGroup.selectedIndex = 1;
    }
    // tslint:disable-next-line:one-line
    else { this.selectedTab = 0; }
  }

  selSelectedTab(): void {
    this.route.params.subscribe(
      data => {
        if (data['option']) {
          this.option = data['option'];
          this.selectTab(this.option);
        }
      });
  }

  onTabSelect(): void {
    this.tabGroup.selectedIndexChange.subscribe(data => {
      if (data === 1) {
        this.option = 'list'; this.selectTab(this.option);
        this.onResetDataTable();
      }
      if (data === 0) { this.option = 'novi'; this.selectTab(this.option); }
    });
  }

  onResetDataTable(first = true) {
    this.isLoading = true; // Progress bar trigger
    if (!super.firstPagePagination(this.paginator, first)) { // List reload on selected page
      this.lgs.getData(true).subscribe(
        data => {
          const languages = super.resolveResult(data);
          if (languages) {
            console.log(languages);
            this.lgs.initPaginator(this.paginator, languages);
            this.subTableData$.next(languages['data']);
          }
          this.isLoading = false;
        });
    }
  }

}


export class ContentDataSource extends DataSource<any> {
  constructor(private kupci$: Observable<ILanguage[]>) {
    super();
  }

  connect(): Observable<ILanguage[]> {
    return this.kupci$;
  }

  // updateTabel(index: number) {
  //   this.kupci$.subscribe(data => {
  //     data[index].isActive === 0 ? data[index].isActive = 1 : data[index].isActive = 0;
  //   });
  // }

  disconnect() { }
}
