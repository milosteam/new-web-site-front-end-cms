import { Component, OnInit, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { IBaseFunctionality, IBasePagination, IBaseTab } from '../shared/models/interfaces/common';
import { FormBuilder, Validators, FormArray, FormGroupDirective, Form } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { MatSnackBar, MatTabGroup, MatPaginator } from '@angular/material';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IText, TEXTTYPE, TEXT_PARENT } from '../shared/models/texts/text';
import { DataSource } from '@angular/cdk/table';
import { TextService } from './text.service';
import { FILEURL } from 'src/app/shared/globals';
import { FileuploadComponent } from 'src/app/shared/components/fileupload/fileupload.component';
import { LangService } from '../settings/languages/lang.service';
import { ILanguage } from '../shared/models/langauge/langauge';
import { tap, map } from 'rxjs/operators';
import { IImageValidator } from '../shared/models/images/image';

@Component({
  selector: 'app-texts',
  templateUrl: './texts.component.html',
  styleUrls: ['./texts.component.scss']
})

export class TextsComponent extends BasecomponentComponent
  implements OnInit, AfterViewInit, IBaseFunctionality, IBasePagination, IBaseTab {

  PARENT = TEXT_PARENT;
  FILEURL = FILEURL;

  //  Data Table
  subTableData$ = new BehaviorSubject([]);
  tableData$ = new ContentDataSource(this.subTableData$.asObservable());
  headerColumns = ['position', 'text_type', 'text_paragraphs', 'extra', 'edit'];
  //

  updateModel: IText = null;
  copyModel: IText = null;
  isLoading = false;

  @ViewChild('textTab', { static: true }) tabGroup: MatTabGroup;
  selectedTab = 1;
  option: string;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  objPar = {
    title_par: { value: '', disabled: false },
    text: { value: '', disabled: false },
  };

  newText = this.fb.group({
    text_type: ['line', [Validators.required]],
    text_name: [''],
    text_title: [''],
    paragraphs: this.fb.array([super.objectToFormControl(this.objPar)]),
    id_language: ['', Validators.required],
    katImage: this.fb.array([
    ]),
    parent: [this.PARENT, [Validators.required]]
  });

  @ViewChild('fuc', { static: false }) fuc: FileuploadComponent;
  imageSettings: IImageValidator = { numOfImages: 4, allowedExt: ['jpg', 'png', 'jpeg', 'gif'] }
  imageSrcs = [];

  texttype = TEXTTYPE;
  languages$: Observable<ILanguage>;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    protected fus: FileuploadService,
    protected snackBar: MatSnackBar,
    private txs: TextService,
    private lgs: LangService,
    private changeDetectorRef: ChangeDetectorRef,
  ) { super(fus, snackBar); }


  ngOnInit(): void {
    this.selSelectedTab();
    this.languages$ = this.lgs.getData(false).pipe(
      map(data => data['data'])
    );
  }

  ngAfterViewInit(): void {
    this.onTabSelect();
    this.uploadImageEnable();
  }

  uploadImageEnable() {
    this.newText.get('text_type').valueChanges.subscribe(
      type => {
        if (type === 'rich') {
          this.changeDetectorRef.detectChanges();
          if (this.fuc) {
            this.fuc.$exportFiles.subscribe(file => {
              if (file) {
                this.imageSrcs = [];
                super.readURL(this.imageSrcs, <FileList>file, this.newText.get('katImage') as FormArray);
              } else {
                this.imageSrcs = [];
                (this.newText.get('katImage') as FormArray).reset();
              }
            });
            this.fuc.setUploadSettings(this.imageSettings);
          }
        }
      });
  }

  onFormSubmit(formGroup?: FormGroupDirective): void {
    if (this.newText.dirty && this.newText.valid) {
      this.isLoading = true;
      const DATA = super.jsonToFormData(this.newText);
      DATA.append('text_paragraphs', JSON.stringify(this.newText.get('paragraphs').value));
      this.txs.newData(DATA).subscribe(
        result => {
          super.resolveResult(result, "Uspešno ste dodali novi tekst");
          this.isLoading = false;
        });
    }
  }


  modelForUpdate(text: IText) {
    if (text) {
      this.updateModel = text;
      this.updateModel.parent = TEXT_PARENT;
      this.updateModel.paragraphs = JSON.parse(this.updateModel.text_paragraphs);
      console.log(this.updateModel);
      super.objectToForm(this.newText, this.updateModel);
      this.newText.markAsDirty();
      this.option = 'novi';
      this.selectTab(this.option);
    }
  }

  onFormUpdate(formGroup?: FormGroupDirective): void {
    if (this.newText.valid && this.newText.dirty) {
      this.isLoading = true;
      const DATA = this.newText.value;
      DATA.text_paragraphs= JSON.stringify(this.newText.get('paragraphs').value);
      this.txs.updateData(DATA, this.updateModel.id_text).subscribe(
        result => {
          super.resolveResult(result, 'Uspešno ste izmenili tekst.');
          this.isLoading = false;
          this.onResetFrom(formGroup);
        });

    }
  }

  onResetFrom(formGroup?: FormGroupDirective): void {
    this.updateModel ? this.updateModel = null : false;
    formGroup ? formGroup.reset() : false;
    this.newText.reset();
    this.newText.get('text_type').setValue('line');
    this.newText.get('parent').setValue(this.PARENT);
    super.removeFromArrayLimit(<FormArray>this.newText.get('paragraphs'));
    this.isLoading === true ? this.isLoading = false : false;
  }

  onPaginationChange(event: import("@angular/material").PageEvent): void {
    if (event) {
      this.lgs.paginatorUpdate(event);
      this.onResetDataTable();
    }
  }


  selectTab(tab: string): void {
    if (tab === 'novi') {
      this.tabGroup.selectedIndex = 0;
    }
    // tslint:disable-next-line:one-line
    else if (tab === 'list') {
      if (this.newText.dirty) {
        this.onResetFrom();
      }
      this.tabGroup.selectedIndex = 1;
    }
    // tslint:disable-next-line:one-line
    else { this.selectedTab = 0; }
  }

  selSelectedTab(): void {
    this.route.params.subscribe(
      data => {
        if (data['option']) {
          this.option = data['option'];
          this.selectTab(this.option);
        }
      });
  }

  onTabSelect(): void {
    this.tabGroup.selectedIndexChange.subscribe(data => {
      if (data === 1) {
        this.option = 'list'; this.selectTab(this.option);
        this.onResetDataTable();
      }
      if (data === 0) { this.option = 'novi'; this.selectTab(this.option); }
    });
  }

  onResetDataTable(first = true) {
    this.isLoading = true; // Progress bar trigger
    if (!super.firstPagePagination(this.paginator, first)) { // List reload on selected page
      this.txs.getData(true).subscribe(
        result => {
          const texts = super.resolveResult(result);
          if (texts) {
            console.log(texts);
            this.txs.initPaginator(this.paginator, texts);
            this.subTableData$.next(texts['data']);
          }
          this.isLoading = false;
        });
    }
  }
}

export class ContentDataSource extends DataSource<any> {
  constructor(private kupci$: Observable<IText[]>) {
    super();
  }

  connect(): Observable<IText[]> {
    return this.kupci$;
  }

  // updateTabel(index: number) {
  //   this.kupci$.subscribe(data => {
  //     data[index].isActive === 0 ? data[index].isActive = 1 : data[index].isActive = 0;
  //   });
  // }

  disconnect() { }
}
