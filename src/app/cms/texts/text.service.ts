import { Injectable } from '@angular/core';
import { ICrudService } from '../shared/models/interfaces/common';
import { Observable } from 'rxjs';
import { MatPaginator } from '@angular/material';
import { RequestService } from 'src/app/shared/services/request.service';
import { APPURL } from 'src/app/shared/globals';
import { IText } from '../shared/models/texts/text';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class TextService implements ICrudService {

  constructor(private rs: RequestService) { }

  newData(dataObject: any): Observable<any> {
    return this.rs.post(APPURL + '/text', dataObject).pipe(
      map(data => <IText>data)
    );
  }
  
  updateData(dataObject: any, id: number): Observable<any> {
    return this.rs.put(APPURL + '/text/' + id, dataObject).pipe(
      map((text : IText) => text)
    );
  }

  getData(pagination=true, filter?: any): Observable<any> {
    return this.rs.get(APPURL + '/text', pagination, filter).pipe(
      map(data => <IText[]>data['data']));
  }

  paginatorUpdate(data: any) {
    this.rs.setPaginatorData(data);
  }

  initPaginator(paginator: MatPaginator, data: any) {
    this.rs.setPaginator(paginator, data);
  }
}
