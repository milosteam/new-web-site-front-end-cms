import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { distinctUntilChanged, debounceTime, map } from 'rxjs/operators';
import { MatOptionSelectionChange } from '@angular/material';
import { IText } from 'src/app/cms/shared/models/texts/text';
import { TextService } from '../../text.service';

@Component({
  selector: 'app-textsearch',
  template: `
  <mat-form-field class="col-12">
      <input type="text" placeholder="Unesite naziv teksta" aria-label="Number" matInput [formControl]="text"
          [matAutocomplete]="auto">
          <mat-autocomplete #auto="matAutocomplete">
          <mat-option *ngFor="let option of filteredTexts$ | async; let i = index" [value]="option?.text_name"
          (onSelectionChange)="onTextSelected($event, option)">
            {{option?.text_name}}
          </mat-option>
        </mat-autocomplete>
    </mat-form-field>
    <mat-progress-bar mode="indeterminate" *ngIf="isLoading"></mat-progress-bar>
    `,
  styles: []
})
export class TextsearchComponent implements OnInit {

  @Input() txt_type: string
  @Output() txt: EventEmitter<IText[]> = new EventEmitter();

  subscription: Subscription;
  text: FormControl = new FormControl('');
  selected: IText;
  isLoading = false;
  filteredTexts$: Observable<IText[]>;

  constructor(private txs: TextService) { }

  ngOnInit() {
    console.log(this.txt_type);
    this.subscription = this.text.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(1000),
        map(text => this._filter(text))
      )
      .subscribe();
  }

  onTextSelected(event: MatOptionSelectionChange, text: IText) {
    if (event && event.isUserInput) {
      this.selected = text;
      this.txt.emit([this.selected]);
    }
  }

  private _filter(value: string): IText[] {
    const filterValue = value.toLowerCase().trim();
    console.log(filterValue);
    const query = !this.selected || this.selected.text_name.toLowerCase().trim() !== filterValue;
    if (query) {
      const filtered = [];
      if (filterValue && filterValue !== '') {
        this.isLoading = true;
        const search = this.txt_type ? [{ 'text_name': filterValue}, {'text_type': this.txt_type }] : [{ 'text_name': filterValue }];
        this.filteredTexts$ = this.txs.getData(true, search).pipe(map(data => {
          if (data['data']) { this.isLoading = false; return data['data']; } else { this.isLoading = false; return []; }
        })
        );
      } else { this.isLoading = false; return filtered; }
    }
  }

  resetSrch() {
    this.text.setValue('');
    this.selected = null;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
