import { Injectable } from '@angular/core';
import { ICrudService } from '../shared/models/interfaces/common';
import { RequestService } from 'src/app/shared/services/request.service';
import { Observable } from 'rxjs';
import { APPURL } from 'src/app/shared/globals';
import { IPage } from '../shared/models/pages/page';
import { map, tap } from 'rxjs/operators';
import { MatPaginator } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class PageService implements ICrudService {
   constructor(private rs: RequestService) { }

  newData(dataObject: any): Observable<any> {
    return this.rs.post(APPURL + '/page', dataObject).pipe(
      map(data=><IPage>data),
      tap(data=>console.log(data))
    
    )
  }
  updateData(dataObject: any, id: number): Observable<any> {
    throw new Error("Method not implemented.");
  }

  getData(pagination: boolean, filter?: any): Observable<any> {
    return this.rs.get(APPURL + '/page').pipe(
      map(data => <IPage[]>data['data']));
  }

  paginatorUpdate(data: any) {
    this.rs.setPaginatorData(data);
  }

  initPaginator(paginator: MatPaginator, data: any) {
    this.rs.setPaginator(paginator, data);
  }
}
