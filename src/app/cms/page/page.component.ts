import { Component, OnInit, AfterContentInit, AfterViewInit, ViewChild } from '@angular/core';
import { IBaseFunctionality, IBasePagination, IBaseTab } from '../shared/models/interfaces/common';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { MatSnackBar, MatTabGroup, MatPaginator } from '@angular/material';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DataSource } from '@angular/cdk/table';
import { Observable, BehaviorSubject } from 'rxjs';
import { IPage } from '../shared/models/pages/page';
import { PageService } from './page.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent extends BasecomponentComponent
implements OnInit, AfterViewInit, IBaseFunctionality, IBasePagination, IBaseTab {
  //  Data Table
  subTableData$ = new BehaviorSubject([]);
  tableData$ = new ContentDataSource(this.subTableData$.asObservable());
  headerColumns = ['position', 'name', 'type', 'edit'];
 //
  
  updateModel: IPage;
  isLoading = false;

  @ViewChild('pageTab', { static: true }) tabGroup: MatTabGroup;
  selectedTab = 1;
  option: string;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  newPage = this.fb.group({
    page_name:['', [Validators.required]],
    id_page_seo:[''],
    id_social:[''],
    id_link:[''],
    id_page_content:[''],
    active:[false],
    languages:['']
  });

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    protected fus: FileuploadService,
    protected snackBar: MatSnackBar,
    private ps: PageService
  ) {super(fus, snackBar); }
  modelForUpdate(model: any) {
    throw new Error("Method not implemented.");
  }

  
  ngOnInit(): void {
    this.selSelectedTab();
  }
 
  ngAfterViewInit(): void {
    this.onTabSelect();
  }

  

  onFormSubmit(formGroup?: import("@angular/forms").FormGroupDirective): void {
    throw new Error("Method not implemented.");
  }
  onFormUpdate(formGroup?: import("@angular/forms").FormGroupDirective): void {
    throw new Error("Method not implemented.");
  }
  onResetFrom(formGroup?: import("@angular/forms").FormGroupDirective): void {
    throw new Error("Method not implemented.");
  }
  onPaginationChange(event: import("@angular/material").PageEvent): void {
    throw new Error("Method not implemented.");
  }

 
  selectTab(tab: string): void {
    if (tab === 'novi') {
      this.tabGroup.selectedIndex = 0;
    }
    // tslint:disable-next-line:one-line
    else if (tab === 'list') {
      if (this.newPage.dirty) {
        this.onResetFrom(); }
        this.tabGroup.selectedIndex = 1;
    }
    // tslint:disable-next-line:one-line
    else { this.selectedTab = 0; }
  }

   selSelectedTab(): void {
    this.route.params.subscribe(
      data => {
        if (data['option']) {
          this.option = data['option'];
          this.selectTab(this.option);
        }
      });
  }

  onTabSelect(): void {
    this.tabGroup.selectedIndexChange.subscribe(data => {
      if (data === 1) {
        this.option = 'list'; this.selectTab(this.option);
        this.onResetDataTable();
      }
      if (data === 0) { this.option = 'novi'; this.selectTab(this.option); }
    });
  }

  onResetDataTable(first = true) {
    this.isLoading = true; // Progress bar trigger
    if (!super.firstPagePagination(this.paginator, first))
    { // List reload on selected page
      this.ps.getData(true).subscribe(
        data => {
          const contentschemas = super.resolveResult(data);
          if (contentschemas) {
            console.log(contentschemas);
            this.ps.initPaginator(this.paginator, contentschemas);
            this.subTableData$.next(contentschemas['data']);
          }
          this.isLoading = false;
        });
    }
  }

}

export class ContentDataSource extends DataSource<any> {
  constructor(private kupci$: Observable<IPage[]>) {
    super();
  }

  connect(): Observable<IPage[]> {
    return this.kupci$;
  }

  // updateTabel(index: number) {
  //   this.kupci$.subscribe(data => {
  //     data[index].isActive === 0 ? data[index].isActive = 1 : data[index].isActive = 0;
  //   });
  // }

  disconnect() { }
}
