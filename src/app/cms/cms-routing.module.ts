import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CmslandingComponent } from './cmslanding/cmslanding.component';
import { ContentschemasComponent } from './contentschemas/contentschemas.component';
import { PageComponent } from './page/page.component';
import { TextsComponent } from './texts/texts.component';
import { LanguagesComponent } from './settings/languages/languages.component';
import { MenuComponent } from './menu/menu.component';
import { LinkComponent } from './menu/link/link.component';
import {UsersComponent} from "./users/users.component";
import {ReguserComponent} from "./users/reguser/reguser.component";
import {ImagesComponent} from './images/images.component';


const cmsroutes: Routes = [
  {
    path: 'cms',
    component: CmslandingComponent,
    children: [
      {
        path: 'korisnici',
        component: UsersComponent,
        // canActivate: [KoriniciGuard],
        children: [
          {
            path: '',
            component: ReguserComponent
          },
          {
            path: 'reguser',
            component: ReguserComponent
          }]
      },
      {
        path: 'schemas',
        component: ContentschemasComponent
      },
      {
        path: 'pages',
        component: PageComponent
      },
      {
        path: 'menu',
        component: MenuComponent,
        children: [
          
        ]
      },
      {
        path: 'link',
        component: LinkComponent
      },
      {
        path: 'texts',
        component: TextsComponent
      },
      {
        path: 'images',
        component: ImagesComponent
      },
      {
        path: 'langs',
        component: LanguagesComponent
      }
    ]
  }];


@NgModule({
  imports: [RouterModule.forChild(cmsroutes)],
  exports: [RouterModule]
})
export class CmsRoutingModule {


}
