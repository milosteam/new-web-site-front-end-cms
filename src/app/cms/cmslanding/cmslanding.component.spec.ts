import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmslandingComponent } from './cmslanding.component';

describe('CmslandingComponent', () => {
  let component: CmslandingComponent;
  let fixture: ComponentFixture<CmslandingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmslandingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmslandingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
