import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material';
import { Observable } from 'rxjs';
import { IUserinfo } from 'src/app/shared/models/user';
import { UserService } from 'src/app/shared/services/user.service';
import { Router } from '@angular/router';
import { RequestService } from 'src/app/shared/services/request.service';

@Component({
  selector: 'app-cmslanding',
  templateUrl: './cmslanding.component.html',
  styleUrls: ['./cmslanding.component.scss']
})



export class CmslandingComponent implements OnInit {

@ViewChild(MatDrawer) drawer: MatDrawer;
@ViewChild(MatDrawer) login: MatDrawer;
user$: Observable<IUserinfo>;

  constructor(private us: UserService,  private router: Router, private rs: RequestService) { }

  ngOnInit() {
    // this.user$ = this.us.$user;
    // this.user$.subscribe(user => {
    //   if (user === null) {this.router.navigate(['/']); } else if
    //   (user.department === 'kontrola') {
    //     this.router.navigate(['/user/kontrola']);
    //   }
    // });
  }

  closeSideNav() {
    if (this.drawer.opened) {this.drawer.close(); }
    if (this.login.opened) {this.login.close(); }
  }

  onLogOut() {
    this.rs.logout().subscribe(data => this.us.logOutUser());
  }

}
