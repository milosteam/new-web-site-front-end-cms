import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/shared/services/request.service';
import { MatPaginator } from '@angular/material';
import { APPURL } from 'src/app/shared/globals';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  constructor(private rs: RequestService) { }

  paginatorUpdate(data: any) {
    this.rs.setPaginatorData(data);
  }

  initPaginator(paginator: MatPaginator, data: any) {
    this.rs.setPaginator(paginator, data);
  }

  addSeoImage(images: any): Observable<any> {
    return this.rs.post(APPURL + '/addseoimage', images).pipe(
      map((data) => data)
    )
  }

}
