import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { IDataTable, IBaseFunctionality, IBaseTab } from '../shared/models/interfaces/common';
import { BasecomponentComponent } from 'src/app/shared/components/basecomponent.component';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { MatSnackBar, MatTabGroup, MatPaginator, MatBottomSheetRef, MatBottomSheet } from '@angular/material';
import { ImagesService } from './images.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormBuilder, FormControl, FormArray, FormGroup, FormGroupDirective } from '@angular/forms';
import { FileuploadComponent } from 'src/app/shared/components/fileupload/fileupload.component';
import { IImageValidator } from '../shared/models/images/image';
import { ILanguage } from '../shared/models/langauge/langauge';
import { LangService } from '../settings/languages/lang.service';
import { map, ignoreElements } from 'rxjs/operators';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.scss']
})
export class ImagesComponent extends BasecomponentComponent
  implements OnInit, AfterViewInit, IBaseFunctionality, IBaseTab {

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild('fuc', { static: true }) fuc: FileuploadComponent;
  @ViewChild('imageTab', { static: true }) tabGroup: MatTabGroup;

  //Observable Section
  languages$: Observable<ILanguage[]>;

  updateModel: any;

  //  Data Table
  subTableData$ = new BehaviorSubject([]);
  // tableData$ = new ContentDataSource(this.subTableData$.asObservable());
  headerColumns = ['position', 'name', 'type', 'edit'];
  //

  // const newSeoText = {
  //   id_language: { value: language.source.value.id_language, disabled: false },
  //   text_type: { value: 'line', disabled: false },
  //   title_par: { value: '', disabled: false },
  //   text: { value: '', disabled: false }
  // };

  isLoading = false;

  selectedTab = 1;
  option: string;
  imageSettings: IImageValidator = { numOfImages: 4, allowedExt: ['jpg', 'png', 'jpeg', 'gif'] }


  newImage = this.fb.array([

  ]);

  imageLangInfo = this.fb.array([

  ]);

  images = [];

  newSeoImages = this.fb.group({
    imagesarry: this.fb.array([])
  })

  constructor(
    private fb: FormBuilder,
    private ims: ImagesService,
    protected fus: FileuploadService,
    protected snackBar: MatSnackBar,
    private lgs: LangService,
  ) {
    super(fus, snackBar)
  }

  ngOnInit(): void {
    console.log(this.fuc);
    this.fuc.setUploadSettings(this.imageSettings);
    this.fuc.$exportFiles.subscribe(file => this.renderPreviewImage(file));
    this.languages$ = this.lgs.getData(false).pipe(
      map(data => data['data'])
    );

  }

  renderPreviewImage(files) {
    if (files && files[0]) {
      files.map(file => {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = (e) => {
          this.images.push((<FileReader>e.target).result);
          console.log(this.images);
          this.addToImagesFromArray(file);
        }
      });
    }
  }

  addToImagesFromArray(imageFile: File): void {
    const imgarray = <FormArray>this.newSeoImages.get('imagesarry');
    const image = new FormGroup({});
    image.addControl('imageFile', new FormControl());
    console.log(imageFile);
    image.addControl('imageWidth', new FormControl());
    image.addControl('languages', new FormControl());
    image.addControl('imageSeo', new FormArray([]));
    imgarray.push(image);
    console.log(imgarray);
  }

  onImageLang(event, imgIndex: number) {
    let imageSeo: FormArray = <FormArray>(<FormArray>this.newSeoImages.get('imagesarry')).controls[imgIndex].get('imageSeo');
    if (event.value.length === 0) { imageSeo.clear(); }
    if (imageSeo.value.length > event.value.length) {
      imageSeo.value.map((removed, pos) => JSON.stringify(event.value).includes('"id_language":' + removed.id_language) ? true : super.removeFromArr(imageSeo, pos));
    }
    if (imageSeo.value.length < event.value.length) {
      event.value.map(selected => {
        JSON.stringify(imageSeo.value).includes('"id_language":' + selected.id_language) ? true : this.addSeoToImage(selected, imageSeo);
      });
    }
    console.log(this.newSeoImages.value);
  }

  private addSeoToImage(langauge: any, array: FormArray) {
    console.log(langauge);
    const newLineText = {
      id_language: { value: langauge.id_language, disabled: false },
      langugae_name: { value: langauge.language, disabled: true },
      text_type: { value: 'line', disabled: false },
      title_par: { value: '', disabled: false },
      text: { value: '', disabled: false }
    };
    super.addToFormArray(newLineText, array);
  }

  getImageSeo(index: number) {
    return (<FormArray>(<FormArray>this.newSeoImages.get('imagesarry')).controls[index].get('imageSeo')).controls;
  }

  ngAfterViewInit(): void {

  }

  onFormSubmit(formGroup: FormGroupDirective): void {
    if (this.newSeoImages.dirty && this.newSeoImages.valid) {
      const DATA = super.jsonToFormData(this.newSeoImages);
      this.ims.addSeoImage(this.newSeoImages.value).subscribe(data => console.log(data));
    }
  }

  onFormUpdate(formGroup?: import("@angular/forms").FormGroupDirective): void {

  }
  onResetFrom(formGroup?: import("@angular/forms").FormGroupDirective): void {

  }
  modelForUpdate(model: any) {

  }

  selectTab(tab: string): void {

  }
  selSelectedTab(): void {

  }
  onTabSelect(): void {

  }

  onPaginationChange(event: import("@angular/material").PageEvent): void {
    if (event) {
      this.ims.paginatorUpdate(event);
      // this.onResetDataTable();
    }
  }


}


