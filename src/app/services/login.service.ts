import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

// RxJs
import {Observable, throwError} from 'rxjs';
import {map, repeat, catchError} from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url = 'http://localhost/ppback/public/api/login';
  constructor(private http: HttpClient) { }

  login(logindata: {}): Observable<any> {
    return this.http.post(this.url, logindata);
  }
}
