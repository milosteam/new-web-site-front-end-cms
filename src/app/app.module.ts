// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import {SharedModule} from './shared/shared.module';
import {CmsModule} from './cms/cms.module';

import {AppRoutingModule} from './app-routing.module';
// Components
import { AppComponent } from './app.component';
import { MatSnackBar } from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    CmsModule,
    SharedModule,
    HttpClientModule,
    AppRoutingModule
  ],

  providers: [MatSnackBar],
  bootstrap: [AppComponent]
})
export class AppModule { }
