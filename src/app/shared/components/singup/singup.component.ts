import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {SEKTORI} from '../../../shared/globals';

@Component({
  selector: 'app-singup',
  templateUrl: './singup.component.html',
  styleUrls: ['./singup.component.scss']
})
export class SingupComponent implements OnInit {
  isRegistered = true;
  constructor() { }
  readonly sektori = SEKTORI;
  signupform = new FormGroup({
    newusername: new FormControl('' , [Validators.required]),
    newuserfirstname: new FormControl({value: '', disabled: this.isRegistered}, [Validators.required]),
    newuserlastname: new FormControl({value: '', disabled: this.isRegistered}, [Validators.required]),
    sector: new FormControl({value: '', disabled: this.isRegistered}, [Validators.required])
  });

  ngOnInit() {
  }

  onEmailChange() {
    alert("Milos");
  }

}
