import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { map, tap } from 'rxjs/operators';
import { IImageValidator } from 'src/app/cms/shared/models/images/image';

@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.scss']
})
export class FileuploadComponent implements OnInit {

  isOver: BehaviorSubject<boolean> = new BehaviorSubject(false);
  $files: BehaviorSubject<File[] | FileList> = new BehaviorSubject(null);
  uploadSettings: IImageValidator;
  public $exportFiles: Observable<File[] | FileList> = this.$files.asObservable();
  uploadFilesControl = new FormControl();

  constructor() { }

  ngOnInit() {
  }

  onMouseOver(over: boolean) {
    over === true ? this.isOver.next(true) : this.isOver.next(false);
  }

  onFilesAdded(data) {
    console.log(data);
    if (data && data.files && data.files[0]) {
      this.$files.next(this.validateSettings(data.files));
    } else {
      this.$files.next(null);
    }
  }

  onUploadClick() {
    var input = document.createElement('input');
    input.type = 'file';
    input.multiple = true;
    input.click();
    input.addEventListener('change', (e) => {
      if ((<HTMLInputElement>e.target).files && (<HTMLInputElement>e.target).files[0]) {
        this.$files.next(this.validateSettings((<HTMLInputElement>e.target).files));
      }
    });
  }

  validateSettings(files: FileList) {
    let uploadFiles: File[] = [];
    if (this.uploadSettings) {
      Array.from(files).map(file => {
        this.uploadSettings.allowedExt.includes(file.type.split('/')[1]) ? uploadFiles.push(file) : false;
      })
      return uploadFiles;
    }
  }

  removeFileUpload(): void {
    this.$files.next(null);
  }

  setUploadSettings(settings: {}): void {
    this.uploadSettings = settings;
  }

}
