import { Directive, EventEmitter, HostListener, Output, ElementRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Directive({
  selector: '[appFiledrop]'
})
export class FiledropDirective {

  @Output() addedFiles = new EventEmitter <FileList>();
  @Output() hoveringFiles = new EventEmitter <boolean>();
 

  
  constructor(private el: ElementRef) { }

    @HostListener('drop', ['$event']) onDrop ($event) 
    { 
      $event.preventDefault();
      const addedfiles = $event.dataTransfer;
      this.addedFiles.emit(addedfiles);
    } 

    
    @HostListener('dragover', ['$event']) onDragOver($event) 
    { 
      $event.preventDefault();
      this.hoveringFiles.emit(true);
    } 

    @HostListener('dragleave', ['$event']) onDragLeave($event) 
    { 
      $event.preventDefault();
      this.hoveringFiles.emit(false);
    } 
}
