import { Component, OnInit } from '@angular/core';
import { IUserinfo } from 'src/app/shared/models/user';
import { FormControl, FormGroup, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher, MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../services/user.service';
import { RequestService } from '../../../services/request.service';
import { Observable, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';



@Component({
  selector: 'app-passreset',
  templateUrl: './passreset.component.html',
  styleUrls: ['./passreset.component.scss']
})
export class PassresetComponent implements OnInit {

  updateUser: IUserinfo;

  isTokenized$: Observable<string> = null;
  subIsTokenized: Subscription;
  isLoading = false;

  newPass = {
    token: null,
    password: null,
    password_confirmation: null,
    email: null
  };

  resetPassForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
    confirmpassword: new FormControl('', Validators.required)
  });

  reqPassResForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email])
  });

  constructor(
    private router: Router,
    private aroute: ActivatedRoute,
    private us: UserService,
    private rs: RequestService,
    public snackBar: MatSnackBar) { }

  ngOnInit() {
    
    // this.adminUser$ =  this.us.$user.pipe(map(user => user.userRole === 0 ? user : null ));
    
    this.isTokenized$ = this.aroute.queryParams.pipe(
       map(token => {if (token['token'] && <string> token['token'] !== '')
        {this.newPass.token = token['token']; return token['token']} else {return null; } })
      );
  }

  onLoginCancle() {
    this.router.navigate(['']);
  }

  sendResetRequest() {
    if (this.reqPassResForm.dirty && this.reqPassResForm.valid === true) {
      this.rs.sendResPassReq(this.reqPassResForm.value).subscribe(
        data => {if (!data['error']) {
          this.snackBar.open('Proverite imejl, da biste dovršili proces promene pristupne šifre.', 'OK', {
            duration: 4000,
          }).afterDismissed().subscribe(done => this.router.navigate(['']));
        }}
      );
    }
  }

  onResetPassSubmit() {
    if (this.resetPassForm.dirty && this.resetPassForm.valid) {
      this.isLoading = true;
      this.newPass.password = this.resetPassForm.controls['password'].value;
      this.newPass.password_confirmation = this.resetPassForm.controls['confirmpassword'].value;
      this.newPass.email = this.resetPassForm.controls['email'].value;
      this.rs.sendResPass(this.newPass).subscribe(data => {
        if (!data['error']) {
          this.isLoading = false;
            this.snackBar.open('Uspešno ste promenili vašu pristupnu šifru.', 'OK', {
              duration: 4000,
            }).afterDismissed().subscribe(done => this.router.navigate(['']));
        } else {
          this.snackBar.open('Došlo je do greške prilikom kreiranja nove pristupne šifre.', 'OK', {
            duration: 4000,
          }).afterDismissed().subscribe(done => this.router.navigate(['']));
          this.isLoading = false;
        }
      });
    }
  }

  resetForms() {
    this.resetPassForm.reset();
    this.reqPassResForm.reset();
  }

}

export class RepeatPasswordEStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return (control && control.parent.get('password').value !== control.parent.get('confirmpassword').value && control.dirty);
  }
}
