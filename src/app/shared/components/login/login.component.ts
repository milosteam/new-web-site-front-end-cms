import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RequestService } from '../../services/request.service';
import { UserService } from '../../../shared/services/user.service';
import { IUserinfo } from '../../models/user';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hide = true;
  isLoading = false;
  falseLogin = 0;

  constructor(
    private rs: RequestService,
    private us: UserService,
    private router: Router,
    public snackBar: MatSnackBar
  ) { }

  loginform = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required])
  });

  ngOnInit() {
  this.us.$user.subscribe(user => {
    if (user !== null) {
      console.log(user);
      this.navigateUser(user);
    }
  });
  }

  login() {
    if (this.loginform.valid) {
      this.isLoading = true;
      this.disableInput();
      this.rs.login(this.loginform.value)
        .subscribe(data => {
          if (data['data'] && data['data']['userinfo']) {
            const user = <IUserinfo> data['data']['userinfo'];
            this.us.setLogedUser(user);
            this.navigateUser(user);
          } else {
            this.snackBar.open('Uneti pristupni podaci nisu korektni.', 'OK', {
              duration: 4000,
            });
            this.falseLogin++;
          }
          this.isLoading = false; this.enableInput();
       });
    }
  }

  private navigateUser(user) {
  if (user.userRole >= 3 ) {
      {this.router.navigate(['/admin']); }
    } else { this.router.navigate(['/user']); }
  }

  private disableInput() {
    this.loginform.disable();
  }

  private enableInput(){
    this.loginform.controls['password'].setValue('');
    this.loginform.enable();

  }

  private navigateToAdmin() {
    this.loginform.value.username === 'admin' ? this.router.navigate(['/admin']) : false;
  }
}
