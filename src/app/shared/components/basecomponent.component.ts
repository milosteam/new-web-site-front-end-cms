import { Component, OnInit, ɵNOT_FOUND_CHECK_ONLY_ELEMENT_INJECTOR } from '@angular/core';
import { FileuploadService } from 'src/app/shared/services/fileupload.service';
import { FormArray, FormControl, FormGroup, AbstractControl, Form } from '@angular/forms';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { FILEURL, ROOT } from '../globals';

@Component({
  selector: 'app-basecomponent',
  template: '',
  styleUrls: ['./basecomponent.component.scss']
})
export class BasecomponentComponent implements OnInit {

  protected FILEURL = FILEURL;
  protected ROOT = ROOT;

  constructor(
    protected fus: FileuploadService,
    protected snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  protected uploadImage(grupa: any) {
    grupa.isLoading = true;
    const input = document.createElement('input');
    input.type = 'file';
    input.multiple = true;
    input.click();
    input.addEventListener('change', (e) => {
      if ((<HTMLInputElement>e.target).files && (<HTMLInputElement>e.target).files[0]) {
        const formData = new FormData;
        formData.append('grupa', JSON.stringify(grupa));
        formData.append('katImage', (<HTMLInputElement>e.target).files[0]);
        this.fus.addFile(formData).subscribe(data => {
          if (!data['error']) {
            grupa.files.push(data['data']);
            input.remove();
          }
          grupa.isLoading = false;
        });
      }
    });
  }

  protected replaceImage(grupa: any, oldimage: any) {
    this.uploadImage(grupa);
    this.removeImage(grupa, oldimage);
  }

  protected removeImage(grupa: any, image: any) {
    grupa.isLoading === false ? grupa.isLoading = true : false;
    const data = { idParent: grupa.idGrupaOpreme, file: image }
    this.fus.deleteFile(data).subscribe((data: any) => {
      if (!data['error']) {
        grupa.files.splice(grupa.files.indexOf(image), 1);
      }
      grupa.isLoading = false;
    })
  }

  protected createImage(formImageArray: FormArray, image: File, width?: string, height?: string, newfilename?: string): void {
    const images = formImageArray as FormArray;
    const newimage = new FormControl();
    newimage.setValue(image);
    images.push(newimage);
  }

  protected readURL(imgSrcs, files, formImageArray: FormArray) {
    if (files && files[0]) {
      for (let file of (<File[]>files)) {
        {
          var reader = new FileReader();
          reader.readAsDataURL(file);
          this.createImage(formImageArray, file);
          reader.onload = (e) => {
            imgSrcs.push((<FileReader>e.target).result);
          }
        }
      }
    } else {
      return null;
    }
  }

  imagesToFromData(formData: FormData, formArray: FormArray) {
    let i = 0;
    for (let image of formArray.value) {
      if (image && image.name) {
        formData.append('katImage[]', image, image.name);
      }
    }
  }

  protected objectToFormControl(object: any): FormGroup {
    try {
      const formGroup = new FormGroup({});
      Object.keys(object).forEach(key => {
        if (object[key] && object[key].value && object[key].disabled) {
          // tslint:disable-next-line:max-line-length
          formGroup.addControl(key, new FormControl({ value: object[key].value, disabled: object[key].disabled ? object[key].disabled : false }));
        } else if (object[key] !== '') {
          formGroup.addControl(key, new FormControl(object[key]));
        }
      });
      return formGroup;
    }
    catch (err) {
    }

  }

  addToFormArray(object: any, formArry: FormArray | AbstractControl): void {
    if ((<FormArray>formArry).length < 10) {
      (<FormArray>formArry).push(this.objectToFormControl(object));
    }
  }

  removeFromArr(formArry: FormArray, i: number): void {
    console.dir(formArry);
    if (formArry instanceof FormArray && formArry.controls[i]) {
      formArry.removeAt(i);
    }
  }

  removeFromArrayLimit(formArry: FormArray, limit = 1): void {
    if (formArry.length > limit) {
      while (formArry.length  > limit) {
        formArry.removeAt(formArry.length-1);
      }
    }
  }

  objectToForm(form: FormGroup, data: any, formObject?: any) {
    const populateForm = (form: FormGroup | FormArray) => {
      if (form && data) {
        for (let control in form.controls) {
          if (form.controls[control] instanceof FormControl) {
            data[control] ? form.controls[control].setValue(data[control]) : false;
          } else if (form.controls[control] instanceof FormArray) {
            form.controls[control].controls = [];
            if (data[control] && data[control].length > 0) {
              for (const cont of data[control]) {
                if (cont && typeof (cont) === 'object') {
                  if (formObject) { this.comapreTwoObj(cont, formObject); }
                  console.log(cont);
                  form.controls[control].push(this.objectToFormControl(cont));
                }
              }
              // form.controls[control].push(objectToFormControl(data[control]));
            }
          }
        }
      }
    };
    populateForm(form);
  }


  protected comapreTwoObj(objData: any, objForm: any) {
    Object.keys(objForm).forEach(key => {
      if (objData[key] === undefined) { objData[key] = objForm[key]; }
    });
  }



  protected jsonToFormData(object: FormGroup): FormData {
    let data = new FormData;
    appendFormVontroll(data, object);
    function appendFormVontroll(data: FormData, object: FormGroup | FormArray, arr?: string) {

      for (const control in object.controls) {
        if (!(object.controls[control] instanceof FormArray) && object.controls[control].valid) {
          arr ?
            data.append(arr + '[]', object.controls[control].value) :
            data.append(control, object.controls[control].value);
        }
        else if (object.controls[control] instanceof FormArray) {
          appendFormVontroll(data, object.controls[control], control);
        }
      }
    }
    return data;
  }

  protected jsonStringToArray(json: any) {
    try {
      if (typeof (json) === 'string') {
        return JSON.parse(json);
      } else {
        return json;
      }
    }
    catch (e) {

    }
  }

  protected dateToTimeStamp(date: Date): string {
    if (date) {
      const tmpDate = new Date(date.getTime());
      tmpDate.setDate(tmpDate.getDate() + 1); //Fix for wrong day()
      // tslint:disable-next-line:max-line-length
      return tmpDate.getUTCFullYear() + '-' + this.twoDigits((tmpDate.getUTCMonth() + 1)) + '-' + this.twoDigits(tmpDate.getUTCDate()) + ' ' + '00' + ':' + '00' + ':' + '00';


    }
  }

  private twoDigits(d) {
    if (0 <= d && d < 10) { return "0" + d.toString(); }
    if (-10 < d && d < 0) { return "-0" + (-1 * d).toString(); }
    return d.toString();
  }

  protected resolveResult(data: any, msg?: string) {
    if (!data) {
      this.snackBar.open('Došlo je do greške, pokušajte ponovo.',
        'OK', {
        panelClass: 'error',
        duration: 10000,
      }); return null;
    }
    if (data && data['error']) {
      this.snackBar.open('Došlo je do greške. ERROR CODE: ' + data['error']['error'],
        'OK', {
        panelClass: 'error',
        duration: 10000,
      }); return null;
    }
    if (data && data['data']) {
      if (msg) {
        this.snackBar.open(msg, 'OK', {
          duration: 2000
        });
      }
      return data;
    }
  }

  protected alertMsg(msg: string, err = '') {
    this.snackBar.open(msg + err,
      'OK', {
      panelClass: 'error',
      duration: 10000,
    });
  }

  public returnFormArray(formArray: FormArray | AbstractControl) {
    if (formArray && formArray instanceof FormArray) {
      return <FormArray>formArray;
    }
  }

  protected firstPagePagination(paginator, first) {
    if (paginator && paginator.pageIndex > 0 && first === true) {
      paginator.firstPage();
      return true;
    } else { return false }
  }

}
