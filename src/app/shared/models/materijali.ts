export const MATERIJALI = [
  {
    'grupa': 'OPP',
    'materijali': [
      { ime: 'MAT.OPP.20', gram: 18.00 },
      { ime: 'MAT.OPP.25', gram: 22.25 },
      { ime: 'OPP.15', gram: 13.65 },
      { ime: 'OPP.20', gram: 18.20 },
      { ime: 'OPP.25', gram: 22.75 },
      { ime: 'OPP.30', gram: 27.30 },
      { ime: 'OPP.35', gram: 31.9 },
      { ime: 'OPP.40', gram: 36.40 },
      { ime: 'OPP.LTS.30', gram: 27.30 },
      { ime: 'OPP.LTS.35', gram: 31.90 },
      { ime: 'OPP.LTS.40', gram: 36.40 },
      { ime: 'OPP.LTN.30', gram: 27.30 },
      { ime: 'OPP.LTN.35', gram: 31.90 },
      { ime: 'OPP.LTN.40', gram: 36.40 },
      { ime: 'OPP.CLS.25', gram: 22.40 },
      { ime: 'OPP.CLS.30', gram: 27.00 },
      { ime: 'OPP.RILIZ.NNH.20', gram: 18 },
      { ime: 'OPP.RILIZ.PLX.20', gram: 18 },
      { ime: 'OPP.RILIZ.NND.20', gram: 18 },
      { ime: 'OPP.MET.18', gram: 16.30 },
      { ime: 'OPP.MET.20', gram: 18.20 },
      { ime: 'OPP.MET.25', gram: 22.80 },
      { ime: 'OPP.MET.30', gram: 27.30 },
      { ime: 'OPP.BELI.20', gram: 19.30 },
      { ime: 'OPP.BELI.30', gram: 29.00 },
      { ime: 'OPP.BELI.35', gram: 33.8 },
      { ime: 'OPP.BELI.40', gram: 38.6},
      { ime: 'OPP.TPE.20', gram: 18.20 },
      { ime: 'OPP.XTMH.20', gram: 18.20 },
      { ime: 'OPP.XTMH.30', gram: 27.30 },
      { ime: 'OPP.DO447.38', gram: 20.50 },
      { ime: 'OPP.MET.MM388.18', gram: 16.30 },
      { ime: 'OPP.SED.LGL.38', gram: 23.6 },
      { ime: 'OPP.SED.LGL.47', gram: 29.1 },
      { ime: 'OPP.SED.MET.LZL.38', gram: 23.60 },
      { ime: 'OPP.SED.MET.LZL.47', gram: 29.10 },
      { ime: 'OPP.SED.MET.QCS.35', gram: 23.5 },
      { ime: 'OPP.SED.MET.LW280.38', gram: 23.6 },
      { ime: 'OPP.SED.ERD.28', gram: 20.8 },
      { ime: 'OPP.SED.ERD.35', gram: 26.0 },
      { ime: 'OPP.SED.ERD.40', gram: 29.5 },
      { ime: 'OPP.SED.ESK.35', gram: 24.5 },
      { ime: 'OPP.SED.ESK.40', gram: 29.0 },
      { ime: 'OPP.SED.HGP.34', gram: 24.5 },
      { ime: 'OPP.SED.HGP.40', gram: 28.8 },
      { ime: 'OPP.SED.LWD.38', gram: 23.5 },
      { ime: 'OPP.SED.PRX.40', gram: 28.3 },
    ]
  },
  {
    'grupa': 'POLIETILEN',
    'materijali': [
      { ime: 'PE.20', gram: 18.4 },
      { ime: 'PE.30', gram: 27.6 },
      { ime: 'PE.40', gram: 36.8 },
      { ime: 'PE.50', gram: 46.0 },
      { ime: 'PE.55', gram: 50.6 },
      { ime: 'PE.60', gram: 55.2 },
      { ime: 'PE.65', gram: 59.8 },
      { ime: 'PE.70', gram: 64.4 },
      { ime: 'PE.75', gram: 69.0 },
      { ime: 'PE.80', gram: 73.6 },
      { ime: 'PE.85', gram: 78.2 },
      { ime: 'PE.90', gram: 82.8 },
      { ime: 'PE.100', gram: 92.0 },
      { ime: 'PE.120', gram: 110.4 },
      { ime: 'PE.SS.30', gram: 27.60 },
      { ime: 'PE.SS.40', gram: 36.8 },
      { ime: 'PE.SS.50', gram: 46.0 },
      { ime: 'PE.SS.60', gram: 55.2 },
      { ime: 'PE.SS.75', gram: 69.00 },
      { ime: 'PE.SS.90', gram: 82.80 },
      { ime: 'PE.BELI.40', gram: 38.6 },
      { ime: 'PE.BELI.50', gram: 48.25 },
      { ime: 'PE.BELI.60', gram: 57.9 },
      { ime: 'PE.BELI.65', gram: 62.72 },
      { ime: 'PE.BELI.70', gram: 67.55 },
      { ime: 'PE.BELI.75', gram: 72.37 },
      { ime: 'PE.BELI.80', gram: 77.2 },
      { ime: 'PE.BELI.90', gram: 86.85 },
      { ime: 'PE.BELI.EX', gram: 61.50 },
      { ime: 'PE.BELI.EX', gram: 75.8 },
      { ime: 'PE.EX.65', gram: 60.8 },
      { ime: 'PE.DP.120', gram: 113.8 },
      { ime: 'PE.BELI.SS', gram: 56.50 },
      { ime: 'PE.MET.NI', gram: 41.40 },
      { ime: 'PE.LAM.AP', gram: 37.10 },
      { ime: 'PE.LAM.AP', gram: 46.40 },
    ]
  },
  {
    'grupa': 'BAREKS', 'materijali':
      [
        { ime: 'PP.EP43.40', gram: 36.8 },
        { ime: 'PP.EP43.50', gram: 45.9 },
        { ime: 'PP.EP43.70', gram: 64.30 },
        { ime: 'PP.EO.70', gram: 64.5 },
        { ime: 'PP.LD53.30', gram: 27.8 },
        { ime: 'PP.LD53.40', gram: 37.0 },
        { ime: 'PP.LD53.55', gram: 50.90 },
        { ime: 'PP.LD53.60', gram: 55.5 },
        { ime: 'PP.LD53.65', gram: 60.12 },
        { ime: 'PP.LD53.70', gram: 64.8 },
        { ime: 'PP.LD53.75', gram: 69.40 },
        { ime: 'PP.LD53.80', gram: 74.0 },
        { ime: 'PP.LD53.85', gram: 78.60 },
        { ime: 'PP.LD53.100', gram: 92.5 },
        { ime: 'PP.EP43.120', gram: 111.0 },
        { ime: 'PE.BELI.ML1361W', gram: 57.9 },
      ]
  },

  {
    'grupa': 'OSTALO', 'materijali': [
      { ime: 'MAT.PET.12', gram: 16.8 },
      { ime: 'PET.12', gram: 16.80 },
      { ime: 'PET.MET.12', gram: 16.80 },
      { ime: 'AL.7', gram: 18.97 },
      { ime: 'AL.9', gram: 24.39 },
      { ime: 'AL.29', gram: 85.30 },
      { ime: 'AL.30', gram: 88.00 },
      { ime: 'AL.37', gram: 107.00 },
      { ime: 'PA.15', gram: 17.4 },
      { ime: 'CPP.25', gram: 22.50 },
      { ime: 'CPP.25', gram: 27.30 },
       {ime: 'MULTIPAK.50/20', gram: 70}
    ]
  }
];

export const VRSTAPROIZVODA = [
  {
    imeGrupe: 'Zbirne sastavnice',
    grupa: [
      { ime: 'ŠTAMPANA TRAKA' },
      { ime: 'DUPLEX' },
      { ime: 'DUPLEX CS' },
      { ime: 'TRIPLEX' },
      { ime: 'KVATRIPLEX' },
      { ime: 'SEČENA ROLNA' }
    ]
  }
];

export const VRSTAPROIZVODA1 = [
  {
    imeGrupe: 'Fazne sastavnice',
     grupa: [
       {ime: 'ŠTAMPANA ROLNA'},
       // {ime: 'KVATRIPLEX / RASTVARAČ'}
     ]
   },
 {
   imeGrupe: 'Sastavnice GP / Rastvarač',
    grupa: [
      {ime: 'TRIPLEX / RASTVARAČ'},
      // {ime: 'KVATRIPLEX / RASTVARAČ'}
    ]
  }
];

export const RESURSI = [
  { ime: 'Heliostar', brzina: 180, tpz: 1.5 },
  { ime: 'Heliostar Cold Seal', brzina: 120, tpz: 1.5 },
  { ime: 'Heliostar bombardovanje', brzina: 120, tpz: 1.5 },
  { ime: 'Primaflex', brzina: 150, tpz: 0.75 },
  { ime: 'Miraflex', brzina: 200, tpz: 0.75 },
  { ime: 'Miraflex bombarodvanje', brzina: 200, tpz: 0.75 },
  { ime: 'Super Combi I hod', brzina: 170, tpz: 0.5 },
  { ime: 'Super Combi II hod', brzina: 110, tpz: 0.5 },
  { ime: 'Super Combi III hod', brzina: 100, tpz: 0.5 },
  { ime: 'Super Combi dupleks', brzina: 200, tpz: 0.5 },
  { ime: 'Super Combi Cold Seal', brzina: 100, tpz: 0.5 },
  { ime: 'Super Simplex I hod', brzina: 170, tpz: 0.5 },
  { ime: 'Super Simplex II hod', brzina: 110, tpz: 0.5 },
  { ime: 'Super Simplex III hod', brzina: 100, tpz: 0.5 },
  { ime: 'Super Simplex dupleks', brzina: 200, tpz: 0.5 },
  { ime: 'Super Simplex bombardovanje', brzina: 180, tpz: 0.5 },
  { ime: 'Slitter 900 GM', brzina: 200, tpz: 0.5 },
  { ime: 'Slitter 900 GM Etikete', brzina: 200, tpz: 0.5 },
  { ime: 'Slitter 900 GM Triplex', brzina: 120, tpz: 0.5 },
  { ime: 'Kampf I mono', brzina: 250, tpz: 0.5 },
  { ime: 'Kampf II mono', brzina: 300, tpz: 0.5 },
  { ime: 'Kampf I dupleks', brzina: 180, tpz: 0.5 },
  { ime: 'Kampf I Triplex', brzina: 120, tpz: 0.5 },
  { ime: 'Kampf I Kvatripleks', brzina: 130, tpz: 0.5 },
  { ime: 'Kampf II dupleks', brzina: 200, tpz: 0.5 },
  { ime: 'Kampf II etikete', brzina: 200, tpz: 0.5 },
  { ime: 'Kampf II Triplex', brzina: 120, tpz: 0.5 },
  { ime: 'Kampf II Kvatripleks', brzina: 130, tpz: 0.5 },
  { ime: 'Kampf II CS', brzina: 110, tpz: 0.5 },
  { ime: 'Premotavanje', brzina: 100, tpz: 0.5 },
  { ime: 'Inkmaker', brzina: 240, tpz: 0.5}
];

export interface IMaterijal {
  ime: string;
  pmasa: number;
  udeo?: number;

  materijal?: any;
}

