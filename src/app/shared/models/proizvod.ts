import {IKupac} from '../models/kupac';
import {IMerenje} from '../models/merenje';
import { IDocumentInfo } from './documetninfo';

interface IMaterijal {
    materijal: string;
    stampa: boolean;
}

export interface IProizvod {
    readonly idProizvoda?: number;
    imeProizvoda: string;
    idKupca: number;
    struktura?: IMaterijal[];
    formaIsporuke: string;
    identPanth?: string;
    idPlana?: number;

    kupac?: IKupac;
    merenje?: IMerenje;
    documentinfo?: IDocumentInfo;
}

