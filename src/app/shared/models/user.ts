
export const USERROLE = ['superadmin', 'admin', 'moderator' , 'user'];

export const USERROLES = {'5':'superadmin' , '4':'admin', '3':'moderator' , '2':'user' };

export const DEPARTMENT = ['finansije', 'komercijala' , 'opriprema', 'tpriprema' , 'kontrola',
    'dizajn', 'sforma', 'magacin' , 'proizvodnja' , 'menadzment', 'odrzavanje', 'informacione tehnologije' ];


export interface IUserinfo {
    idUserInfo?: number;
    userRole: number;
    department?: string;
    username?: string;
    phone?: string;
}

export interface IUser {
    user_id?: number;
    name: string;
    password: string;
    token: string;
    email: string;
    userinfo?: IUserinfo;
}
