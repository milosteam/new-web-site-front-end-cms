import {IProizvod} from '../models/proizvod';
import { IDocumentInfo } from './documetninfo';
import { IMerenje } from './merenje';


export interface IRadninalog {
    idRadnogNaloga?: number;
    radniNalogPanth: string;
    idProizvoda?: number;
    idPlana?: number;
    proizvod?: IProizvod;
    plan?: IMerenje;
    documentinfo?: IDocumentInfo;
}
