export interface IDocumentInfo{
    infoNarucilac: string;
    infoUsluga: string;
    infoErp: string;
    idPlana: number;
    formaIsporuke: string;
}

export const documentInfoObj = {
    infoNarucilac: { value: '', disabled: false },
    infoUsluga: { value: '', disabled: false },
    infoErp: { value: '', disabled: false },
    plan: {value: '', disabled: false },
    formaIsporuke: {value: '', disabled: false}
};

