import { IFile } from './file';

export interface IKategorija {
    readonly idGrupaOpreme?: number,
    nazivGrupeOpreme: string;
    idKategorije?: number;
    isKategorija?: boolean | number;
    napomena: string;
    department?: string;
    idFile?: number;
    file?: IFile
    isLoading?: boolean;
    parent?: string
}

export interface IGrupaOpreme extends IKategorija {
    files?: IFile[]
}

export interface IUredjaj {
    readonly idUredjaja?: number,
    nazivUredjaja: string,
    napomena?: string,
    datumNabavke: Date,
    proizBroj?: string,
    knjigBroj?: string,
    garancija?: number,
    lokacija?: string,
    extraInfo?: Object[] | string,
    isActive?: boolean,
    idGrupeOpreme?: number,
    grupaOpreme?: IGrupaOpreme,
    files?: IFile[]
    parent?: string
}

export interface ISklop{
    readonly idSklop?:number,
    nazivSkopa: string,
    idMasine: number,
    napomena: string,
    files?: IFile[],
    pozicija?:string,
    isActive?: boolean,
    parent?: string
}

export interface IDeo {
    readonly idDela?: number,
    nazivDela: string,
    napomena: string,
    tip: string,
    knjigBroj?: string,
    komada: number,
    idMasine: number,
    idSklopa?: number,
    extraInfo?: Object[] | string,
    isActive?: boolean,
    files?: IFile[],
    parent?: string,
    grupes?: IGrupaOpreme[]
    uredjajs?: IUredjaj[]
}

export const Lokacije = [
    'Hala Proizvodnja',
    'Hala Galvanizacija',
    'Hala Boje',
    'Hala Ekstruzija',
    'Magacin Materijala',
    'Magacin Poluproizvoda',
    'Magacin Konfekcija',
    'Stara Upravna Zgrada - Poslovođe',
    'Stara Upravna Zgrada - Direktor',
    'Stara Upravna Zgrada - Održavanje i Standardi',
    'Stara Upravna Zgrada - Lab',
    'Stara Upravna Zgrada - Kontrola',
    'Nova Upravna Zgrada - Prijem',
    'Nova Upravna Zgrada - Finansije',
    'Nova Upravna Zgrada - Komercijala',
    'Nova Upravna Zgrada - Operativna Priprema',
    'Nova Upravna Zgrada - Dizajn',
    'Održavanje'
]
