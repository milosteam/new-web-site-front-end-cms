import { IUredjaj } from '../oprema';

export interface IIntervencija {
    readonly idIntervencije?: number;
    datumIntervencije: Date;
    opisIntervencije: string;
    idAkcije: number;
    delovi: string;
    napomena: string;
    prijavaKvara?: any;
    vremeZastoja: number;
    zaposleni: string;
    uredjaj: IUredjaj;
}
