export interface IAkcija {
    readonly idAkcije?: number;
    opisAkcije: string;
    sifraAkcije: string;
    akcijaDep: string;
}
