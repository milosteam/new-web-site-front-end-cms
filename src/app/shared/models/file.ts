export interface IFile{
    readonly idFile?: number,
    nameOfFile: string,
    desktopThumbPath: string,
    sizeOfFile: number,
    typeOfFile: string,
    directoryOfFile: string,
    mobThumbPath?:string,
    tabletThumbPath?:string,
    accessLevel?:string,
    singleUserFile?: number,
    napomena?: string
}
