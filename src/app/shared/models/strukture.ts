import { IMaterijal } from './materijali';

export interface IStrukture {
readonly idStrukture?: number;
opisStrukture: string;
oznakaStrukture: string;
struktura?: IMaterijal[];
}
