import {IMaterijal, VRSTAPROIZVODA} from '../models/materijali';

export interface IOznaka {
  opis: string;
  sifra: number;
}

export interface IResurs {
  ime: string;
  brzina: number;
  tpz: number;
}

export interface IFaza extends IInfo {
 oznaka: IOznaka;
 pmasa: number;
 udeo?: number;
 ulazniMaterijali: Array<IMaterijal>;
 resurs: IResurs;
 tn?: number;
 boja?: IMaterijal;
 lepakA?: IMaterijal;
 lepakB?: IMaterijal;
}

export interface IPHod extends IInfo {
  oznaka: IOznaka;
  pmasa: number;
  ident: string;
  kasiranje: IFaza;
}



export interface IMono extends IInfo {
  oznaka: IOznaka;
  pmasa: number;
  inkmaker: IFaza;
  stampa: IFaza;
  secenje: IFaza;
  konfekcioniranje?: IFaza;
  premotavanje?: IFaza;
  prajmerisanje?: IFaza;
  coldseal?: IFaza;
}

export interface IDuplex extends IInfo {
  oznaka: IOznaka;
  pmasa: number;
  inkmaker: IFaza;
  stampa: IFaza;
  kasiranje: IFaza;
  secenje: IFaza;
  konfekcioniranje?: IFaza;
  premotavanje?: IFaza;
  prajmerisanje?: IFaza;
  coldseal?: IFaza;
}

export interface IDuplexColdSeal extends IInfo {
  oznaka: IOznaka;
  pmasa: number;
  inkmaker: IFaza;
  stampa: IFaza;
  kasiranje: IFaza;
  premotavanje: IFaza;
  coldseal: IFaza;
  secenje: IFaza;
  konfekcioniranje?: IFaza;
}

export interface IKasiranaRolna extends IInfo {
  oznaka: IOznaka;
  pmasa: number;
  kasiranje: IFaza;
  secenje: IFaza;
  konfekcioniranje?: IFaza;
  premotavanje?: IFaza;
  prajmerisanje?: IFaza;
  stampa?: IFaza;
  coldseal?: IFaza;
}

export interface ITriplex extends IInfo {
  oznaka: IOznaka;
  pmasa: number;
  prvihod?: IPHod;
  inkmaker: IFaza;
  stampa: IFaza;
  kasiranje: IFaza;
  kasiranje1?: IFaza;
  secenje: IFaza;
  konfekcioniranje?: IFaza;
  premotavanje?: IFaza;
  prajmerisanje?: IFaza;
  coldseal?: IFaza;
}

export interface IKvatriplex extends IInfo {
  oznaka: IOznaka;
  pmasa: number;
  prvihod: IPHod;
  drugihod: IPHod;
  inkmaker: IFaza;
  stampa: IFaza;
  kasiranje: IFaza;
  secenje: IFaza;
  konfekcioniranje?: IFaza;
  premotavanje?: IFaza;
  prajmerisanje?: IFaza;
  coldseal?: IFaza;
}

export interface ISastavnica extends IInfo {
  vrstaProizvoda: string;
  proizvod: IMono | IDuplex | ITriplex | IKvatriplex | IDuplexColdSeal;
}

interface IInfo {
  jedinicaMere: string;
  format: number;
  sirinaTrake?: number;
  korak?: number;
  brojTraka?: number;
  netoTezina?: number;
  jedinicaMetar: number;
}

export const OPERACIJE = 
[
  {nazivop: 'Štampa', oznaka: 'opst' },
  {nazivop: 'Kaširana rolna', oznaka: 'opst' },
  {nazivop: 'Kaširanje PH', oznaka: 'opph' },
  {nazivop: 'Kaširanje DH', oznaka: 'opdh' },
  {nazivop: 'Sečenje', oznaka: 'opsc' },
  {nazivop: 'Cold Seal', oznaka: 'opcs' },
  {nazivop: 'Rasecanje', oznaka: 'oprs' },
];

