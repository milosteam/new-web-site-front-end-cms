export interface IKlise {
    color: string;
    kliseSetNumber: number;
    remark?: string;
}

export interface IColorset {
    colorSetNumber: number;
    kliseSetName: string;
    kliseInNubmer: string;
    klisePPNumber: string;
    kliseListId?: number;
    remark?: string;
}

export interface IKliseList {
    kliseListId?: number;
    kliseList: IKlise[];
}
