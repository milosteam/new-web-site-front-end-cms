import {IUserinfo} from './user';
    import { from } from "rxjs";
export interface IPrijavaKvara {
    readonly idPrijave?: number,
    readonly idEtikete?: string,
    user: IUserinfo,
    idSklopa?: number,
    idMasine: number,
    idDela?: number,
    sklop?: any,
    masina?: any, 
    deo?: any,
    opisKvara:string,
    priorite:number,
}