export interface IKupac {
    readonly idKupca?: number;
    imeKupca: string;
    isActive?: number;
}
