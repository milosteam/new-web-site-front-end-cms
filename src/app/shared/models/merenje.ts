import { NodeCompatibleEventEmitter } from "rxjs/internal/observable/fromEvent";
import { IStrukture } from "./strukture";
import { IRadninalog } from "./radninalog";
import { IUserinfo } from "./user";

const merila = [
  'Mikrometar',
  'Vaga',
  'Organoleptički',
  'Kidalica',
  'Zeva, Kidalica',
  'Lupa',
  'Metar',
  'Metar',
  'Zeva',
  'Šema',
  'Tesa 4104'
]

const ucestalosti = [
  'Svaka rolna',
  '1 uzorak od matične rolne'
]

export const MERENJA = [
    { karakteristika: 'Debljina', karakteristikaen:'Thickness', jm: 'μm', metoda: 'DIN 53370', polje: 'debljina', kontrola: 'debljina', merilo: merila[0], ucestalost: ucestalosti[0]  },
    { karakteristika: 'Gramatura', karakteristikaen:'Unit weight', jm: 'g/m2', metoda: 'DIN 53352', polje: 'gramatura', kontrola: 'gramatura', merilo: merila[1], ucestalost: ucestalosti[0]  },
    { karakteristika: 'Miris', karakteristikaen:'Odour', jm: '0-4', metoda: 'PPM30', polje: 'miris', kontrola: 'miris', merilo: merila[2], ucestalost: ucestalosti[1] },
    {
      karakteristika: 'Koeficijent frikcije unutra/unutra',
      karakteristikaen:'Coefficient of friction inner/ inner',
      jm: 'μ/d', metoda: 'ASTM D 1894', polje: 'frikcunutra', kontrola: 'frikcunutra', merilo: merila[3], ucestalost: ucestalosti[1]
    },
    {
      karakteristika: 'Koeficijent frikcije unutra/met',
      karakteristikaen:'Coefficient of friction inner/ met',
      jm: 'μ/d', metoda: 'ASTM D 1894', polje: 'frikcmet', kontrola: 'frikcmet', merilo: merila[3], ucestalost: ucestalosti[1]
    },
    {
      karakteristika: 'Koeficijent frikcije spolja/spolja',
      karakteristikaen:'Coefficient of friction outer/ outer',
      jm: 'μ/d', metoda: 'ASTM D 1894', polje: 'frikcspolja', kontrola: 'frikcspolja', merilo: merila[3], ucestalost: ucestalosti[1]
    },
    {
      karakteristika: 'Koeficijent frikcije spolja/met',
      karakteristikaen:'Coefficient of friction outer/ met',
      jm: 'μ/d', metoda: 'ASTM D 1894', polje: 'frikcspoljamet', kontrola: 'frikcspoljamet', merilo: merila[3], ucestalost: ucestalosti[1]
    },
    {
      karakteristika: 'Koeficijent frikcije neobr/neobr',
      karakteristikaen:'Coefficient of friction neobr/neobr',
      jm: 'μ/d', metoda: 'ASTM D 1894', polje: 'frikcneob', kontrola: 'frikcneob', merilo: merila[3], ucestalost: ucestalosti[1]
    },
    {
      karakteristika: 'Koeficijent frikcije neobr/met',
      karakteristikaen:'Coefficient of friction neobr/met',
      jm: 'μ/d', metoda: 'ASTM D 1894', polje: 'frikcneobmet', kontrola: 'frikcneobmet', merilo: merila[3], ucestalost: ucestalosti[1]
    },
    {
      karakteristika: 'Jačina međuslojne adhezije - 1',
      karakteristikaen:'Bond strength ___/ ___',
      jm: 'N/15mm', metoda: 'ASTM 882', polje: 'adhezija1', kontrola: 'adhezija1', merilo: merila[3],  ucestalost: ucestalosti[1]
    },
    
    { karakteristika: 'Jačina međuslojne adhezije - 2',
    karakteristikaen:'Bond strength ___/ ___',
    jm: 'N/15mm', metoda: 'ASTM 882', polje: 'adhezija2', kontrola: 'adhezija2', merilo: merila[3],  ucestalost: ucestalosti[1] },
    { karakteristika: 'Jačina međuslojne adhezije - 3',
    karakteristikaen:'Bond strength ___/ ___',
    jm: 'N/15mm', metoda: 'ASTM 882', polje: 'adhezija3', kontrola: 'adhezija3', merilo: merila[3], ucestalost: ucestalosti[1] },
    { karakteristika: 'Jačina vara / COLD SEAL', 
    karakteristikaen:'Heat seal strength',
    jm: 'N/15mm', metoda: 'ASTM 882', polje: 'jacinavara', kontrola: 'jacinavara', merilo: merila[4], ucestalost: ucestalosti[1] },
    { karakteristika: 'Štampa',
    karakteristikaen:'Printing',
    jm: '+', metoda: 'PPM 22', polje: 'stampa', kontrola: 'stampa', merilo: merila[5], ucestalost: ucestalosti[0] },
    { karakteristika: 'Termovarenje',
    karakteristikaen:'Heat seal range',
    jm: '°C', metoda: 'PPM 15', polje: 'termovarenje', kontrola: 'termovarenje', merilo: merila[8], ucestalost: ucestalosti[0] },
    { karakteristika: 'Vizuelni izgled',
    karakteristikaen:'Visual appearance',
    jm: '%', metoda: 'PPM 53', polje: 'vizizgled', kontrola: 'vizizgled', merilo: merila[2], ucestalost: ucestalosti[0] },
    { karakteristika: 'Adhezija boje',
    karakteristikaen:'Ink adhesion',
    jm: '+', metoda: 'PPM 21', polje: 'adhzboje', kontrola: 'adhzboje', merilo: merila[10], ucestalost: ucestalosti[0] },
    { karakteristika: 'Termostabilnost', 
    karakteristikaen:'Termostability',
    jm: '°C', metoda: 'PPM 16', polje: 'termostabilnost', kontrola: 'termostabilnost', merilo: merila[8] , ucestalost: ucestalosti[0]  },
    { karakteristika: 'Stepen sjaja (60°)',
    karakteristikaen:'Gloss (60°)',
    jm: '%', metoda: 'PPM 53', polje: 'stepensjaja', kontrola: 'stepensjaja', merilo: merila[2], ucestalost: ucestalosti[0] },
    { karakteristika: 'Šema hladnog vara - uzdužni',
    karakteristikaen:'Cold seal pattern (MD)',
    jm: 'mm', metoda: 'PPM  23', polje: 'uzduznivar', kontrola: 'uzduznivar', merilo: merila[9], ucestalost: ucestalosti[0] },
    { karakteristika: 'Šema hladnog vara - poprečni',
    karakteristikaen:'Cold seal pattern (TD)',
    jm: 'mm', metoda: 'PPM  23', polje: 'poprecniivar', kontrola: 'poprecniivar', merilo: merila[9], ucestalost: ucestalosti[0] },
  ];


export const EXTRAMERENJE = [
    { karakteristika: 'Širina trake (rolne)',
    karakteristikaen:'Reel width',
    jm: 'mm', metoda: 'PPM 23', polje: 'sirinarolne', kontrola: 'sirinarolne', merilo: merila[7], ucestalost: ucestalosti[0] },
    { karakteristika: 'Širina odreska (korak)',
    karakteristikaen:'Single unit size (height)',
    jm: 'mm', metoda: 'PPM 23', polje: 'korak', kontrola: 'korak', merilo: merila[7], ucestalost: ucestalosti[0] },
    { karakteristika: 'Dimenzija kese',
    karakteristikaen:'Single unit size (width)',
    jm: 'mm', metoda: 'PPM 23', polje: 'dimkese', kontrola: 'dimkese', merilo: merila[7], ucestalost: ucestalosti[0] },
    { karakteristika: 'Prečnik rolne', 
    karakteristikaen:'Reel diameter',
    jm: 'mm', metoda: 'PPM 23', polje: 'precnikrolne', kontrola: 'precnikrolne', merilo: merila[7], ucestalost: ucestalosti[0] },
    { karakteristika: 'Prečnik hilzne',
    karakteristikaen:'Core diameter',
    jm: 'mm', metoda: 'PPM 23', polje: 'precnikhilzne', kontrola: 'precnikhilzne', merilo: merila[7], ucestalost: ucestalosti[0] },
    { karakteristika: 'Prenos',
    karakteristikaen:'Winding direction',
    jm: '/', metoda: 'PPM 24', polje: 'prenos', kontrola: 'prenos', merilo: merila[9], ucestalost: ucestalosti[0] },
];


export interface IMerenje {
    idMerenja?: number;
    nazivplana?: string;
    izdanje?: number;
    oznakaplana?: string;
    debljina?: string;
    gramatura ?: string;
    miris?: string;
    frikcunutra?: string;
    frikcspolja?: string;
    adhezija1?: string;
    adhezija1opis?: string;
    adhezija2?: string;
    adhezija2opis?: string;
    adhezija3?: string;
    adhezija3opis?: string;
    jacinavara?: string;
    termostabilnost?: string;
    stampa?: string;
    termovarenje?: string;
    adhzboje?: string;
    stepensjaja?: string;
    vizizgled?: string;
    uzduznivar?: string;
    poprecniivar?: string;
    sirinarolne?: string;
    zahtevanasirinarolne?: string;
    korak?: string;
    zahtevanakorak?: string;
    dimkese?: string;
    zahtevankorak?: string;
    precnikrolne?: string;
    zahtevanaprecnikrolne?: string;
    precnikhilzne?: string;
    zahtevanaprecnikhilzne?: string;
    prenos?: string;
    zahtevanaprenos?: string;
    idRadnogNaloga?: number;
    isPlan: number;
    isActivePlan: number;
    oznakaPlana?: string;
    izdanjePlana?: number;
    idStrukture?: number;
    napomena?: string;
    struktura?: IStrukture;
    brojPlana: string;
    idUserInfo?: number;
    updated_at?: any;
    radninalog?: IRadninalog;

    userinfo?: IUserinfo;
}
