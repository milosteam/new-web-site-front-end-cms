import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import {
  MatAccordion,
  MatCardModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTabsModule,
  MatAutocompleteModule,
  MatSelectModule,
  MatMenuModule,
  MatProgressBarModule,
  MatSnackBarModule,
  MatSlideToggleModule,
  MatRadioModule,
  MatDialogModule,
  MatTableModule,
  MatListModule,
  MatPaginatorModule,
  MatTooltipModule,
  MatCheckboxModule,
  MatGridListModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatTreeModule,
} from '@angular/material';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { CdkTreeModule } from '@angular/cdk/tree';
import {MatExpansionModule} from '@angular/material/expansion';

import { LoginComponent } from './components/login/login.component';
import { SingupComponent } from './components/singup/singup.component';
import { PassresetComponent } from './components/login/passreset/passreset.component';
import { FileuploadComponent } from './components/fileupload/fileupload.component';
import { FiledropDirective } from './components/fileupload/filedrop.directive';
import { BasecomponentComponent } from './components/basecomponent.component';

import { LandingComponent } from './components/landing/landing.component';

import { NumcommaPipe } from './pipes/numcomma.pipe';

@NgModule({
  imports: [
    MatTreeModule,
    ChartsModule,
    RouterModule,
    MatCardModule,
    MatProgressSpinnerModule,
    CommonModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTableModule,
    MatMenuModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatDialogModule,
    MatTooltipModule,
    MatListModule,
    MatPaginatorModule,
    FormsModule,
    MatCheckboxModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    DragDropModule
  ],
  exports: [
    MatExpansionModule,
    MatTreeModule,
    CdkTreeModule,
    FileuploadComponent,
    PassresetComponent,
    LoginComponent,
    MatCardModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatTabsModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatMenuModule,
    MatProgressBarModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    MatRadioModule,
    MatDialogModule,
    MatListModule,
    MatTableModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    DragDropModule,
    MatPaginatorModule],
  // tslint:disable-next-line:max-line-length
  declarations: [ LoginComponent, SingupComponent, FileuploadComponent, PassresetComponent ]
})
export class SharedModule { }
