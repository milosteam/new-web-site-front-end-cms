export const APPURL = 'http://localhost/newwebsite/backend/public/api';
export const FILEURL  = 'http://localhost/newwebsite/backend/public/api/images/';
export const ROOT = '';

// export const APPURL = 'https://papirprint.biz/api';
// export const FILEURL  = 'https://papirprint.biz/api/images/';
// export const ROOT = 'ppf/';

export const SEKTORI = [
'tehnička priprema',
'kontrola kvaliteta',
'proizvodnja'
];

export const ISPORUKA = [
    'ROLNA',
    'KESA',
    'TABAK'
];

export const JEDMERE = [
    'KG',
    'KOMAD',
    'DUŽNI METAR'
];
