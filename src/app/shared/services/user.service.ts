import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

import { IUserinfo} from '../../shared/models/user';


@Injectable({
  providedIn: 'root'
})

export class UserService {

  private $userSub =  new BehaviorSubject(<IUserinfo>null);
  public $user = this.$userSub.asObservable();
  constructor() { }

  public setLogedUser(user: IUserinfo) {
    if (user) {
      this.$userSub.next(user);
    }
  }

  public logOutUser() {
    this.$userSub.next(null);
  }

}
