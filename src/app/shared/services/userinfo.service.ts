import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { APPURL } from '../globals';
import { map } from 'rxjs/operators';
import { IUserinfo } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserinfoService {

  constructor(private rs: RequestService) { }

  getUserInfo(filter = null) {
    return this.rs.get(APPURL + '/userinfo', true, filter).pipe(
      map((userinfo: IUserinfo[]) => userinfo['data'])
     );
  }

}
