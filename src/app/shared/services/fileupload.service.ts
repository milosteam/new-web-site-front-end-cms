import { Injectable } from '@angular/core';
import { RequestService } from './request.service';
import { APPURL } from '../globals';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileuploadService {

  constructor(private rs: RequestService) { }

  deleteFile(data: any): Observable<boolean> {
    return this.rs.post(APPURL + '/removepivotimage', data).pipe(
      map(data => data)
    )
  }

  addFile(data:any):Observable<any>{
    return this.rs.post(APPURL + '/addpivotimage', data).pipe(
      map(data => data)
    )
  }
  

}
